Информационный телеграм-бот с возможностью управления через веб-интерфейс и поддержкой подписок.

Развертывание:
Python3.7
RabbitMQ==3.6.15

cp information_bot/bot/scripts/actions/basic.dist.py information_bot/bot/scripts/actions/basic.py
cp information_bot/information_bot/settings.dist.py information_bot/information_bot/settings.py
cp .gitignore.dist .gitignore

pip install -r requirements.txt

Заполняем все необходимые настройки в settings.py
Не забываем заполнить SECRET_KEY

python information_bot/manage.py makemigrations
python information_bot/manage.py migrate
python information_bot/manage.py loaddata information_bot/fixtures/days.json
python information_bot/manage.py createsuperuser

Поднять сервер в тестовом режиме:
python information_bot/manage.py runserver 0.0.0.0:port

Запуск бота:
python information_bot/manage.py runscript information_bot

Запуск консьюмера обработки действий по нажатию кнопок в меню:
python information_bot/manage.py runscript button_actions_consumer

Запуск консьюмера обработки действий в подписках:
python information_bot/manage.py runscript subscription_actions_consumer

Пути для favicon и лого:
information_bot/bot/static/media/img/favicon.ico
information_bot/bot/static/media/img/logo.png


