import json

import bot.models as models
import bot.scripts.components.bot as b
import bot.scripts.components.logger as l
import bot.scripts.components.mysql as mysql
import bot.scripts.components.rabbit as r
from django.conf import settings
from telegram import ParseMode


class DataManager:
    def __init__(self, update, context):
        self.update = update
        self.context = context
        self.user_invite = None

        if self.has_callback:
            self.callback_data = json.loads(update.callback_query.data)
        else:
            self.callback_data = {}

    # ############################# БАЗОВЫЕ ИДЕНТИФИКАТОРЫ ####################
    @property
    def chat_id(self):
        """
        Идентификатор чата
        :return: str
        """
        if self.has_callback:
            return self.update.callback_query.message.chat.id
        else:
            return self.update.message.chat.id
    # #########################################################################

    # ############################# РАБОТА С КОЛБЭКОМ #########################
    @property
    def has_callback(self):
        """
        Проверка наличия колбэка
        :return: Boolean
        """
        return bool(self.update.callback_query)

    @property
    def callback_json(self):
        """
        Callback, сформированный в json
        :return: json object
        """
        return json.dumps(self.callback_data)

    @property
    def actions(self):
        """
        Метод получения всех экшенов
        :return: экшены
        """
        actions = models.Buttons.get_actions(self.button_id)
        return actions

    @property
    def interactive_actions(self):
        """
        Метод получения интерактивных экшенов
        :return: интерактивные экшены
        """
        actions = models.Buttons.get_interactive_actions(self.button_id)
        return actions

    @property
    def not_interactive_actions(self):
        """
        Метод получения интерактивных экшенов
        :return: не интерактивные экшены
        """
        actions = models.Buttons.get_not_interactive_actions(self.button_id)
        return actions

    def __last_action_callback_json_getter(self):
        return models.BotUser.active_users.get(chat_identifier=self.chat_id).last_button_callback_json

    def __last_action_callback_json_setter(self, value):
        user = models.BotUser.active_users.get(chat_identifier=self.chat_id)
        user.last_button_callback_json = json.dumps(value)
        user.save()

    def __last_action_callback_json_deleter(self):
        user = models.BotUser.active_users.get(chat_identifier=self.chat_id)
        user.last_button_callback_json = None
        user.save()

    last_action_callback = property(__last_action_callback_json_getter,
                                    __last_action_callback_json_setter,
                                    __last_action_callback_json_deleter,
                                    "Действие для повтора кнопкой repeat (значение должно быть dict)")
    # #########################################################################

    # ############################# ДЕСКРИПТОРЫ КОЛБЭКА #######################
    def __menu_id_getter(self):
        return self.callback_data.get("menu_id")

    def __menu_id_setter(self, value):
        self.callback_data["menu_id"] = value

    menu_id = property(__menu_id_getter, __menu_id_setter, None, "Id меню, в котором находится пользователь")

    def __button_id_getter(self):
        return self.callback_data.get("button_id")

    def __button_id_setter(self, value):
        self.callback_data["button_id"] = value

    button_id = property(__button_id_getter, __button_id_setter, None, "Id нажатой кнопки")

    def __subscription_id_getter(self):
        return self.callback_data.get("subscription_id")

    def __subscription_id_setter(self, value):
        self.callback_data["subscription_id"] = value

    subscription_id = property(__subscription_id_getter, __subscription_id_setter, None, "Id подписки")

    def __subscription_info_id_getter(self):
        return self.callback_data.get("subscription_info_id")

    def __subscription_info_id_setter(self, value):
        self.callback_data["subscription_info_id"] = value

    subscription_info_id = property(__subscription_info_id_getter, __subscription_info_id_setter, None,
                                    "Id подписки (при получении информации о подписке)")
    # ###########################################################################

    # ############################# ДЕСКРИПТОРЫ КОНТЕКСТА #####################
    def __action_id_getter(self):
        action_id = self.context.user_data.get("w_act")
        if action_id:
            del self.context.user_data["w_act"]
        return action_id

    def __action_id_setter(self, value):
        self.context.user_data["w_act"] = value

    action_id = property(__action_id_getter, __action_id_setter, None, "Id ожидаемого экшена")

    # ###########################################################################

    # ############################# РАБОТА С ПОЛЬЗОВАТЕЛЕМ ####################
    @property
    def user_is_registered(self):
        """
        Проверка регистрации пользователя
        :return: boolean
        """
        is_registered = bool(models.BotUser.is_registered(self.chat_id))
        if not is_registered:
            self.user_log("Пользователь не зарегистрирован")
        return is_registered

    @property
    def user(self):
        """
        Сведения о пользователе
        :return: dict | id, first_name, is_bot, last_name, username, language_code
        """
        return self.update.message.from_user

    @property
    def user_fio(self):
        """
        Имя и фамилия пользователя одной строкой (по данным в БД)
        :return: str
        """
        return models.BotUser.get_user_appeal(self.chat_id)

    # ###########################################################################

    # ############################# РАБОТА С ИНВАЙТАМИ ########################

    def has_invite(self):
        """
        Проверяет наличие Инвайта у пользователя
        :return: bool
        """
        return bool(models.Invites.get_invite(invite_code=self.receive_message_text.strip()))

    def get_invite(self, **kwargs):
        """
        Объект инвайта пользователя
        :return: Invite model obj (если существует)
        """
        self.user_invite = models.Invites.get_invite(**kwargs, chat_identifier=self.chat_id)
        return self.user_invite

    def write_invite_chat_id(self):
        """
        Запись chat_id в инвайт
        :return:
        """
        i = self.get_invite(invite_code=self.receive_message_text.strip())
        i.chat_identifier = self.chat_id
        i.save()
        self.user_log(f"Пользователь {self.chat_id} начал подтверждение инвайта")

    def write_invite_inn(self):
        """
        Запись ИНН организации пользователя в инвайт
        :return:
        """
        inn = self.receive_message_text.strip()
        i = self.get_invite()
        i.organization_inn = inn
        i.save()
        self.user_log(f"Пользователь {self.chat_id} указал ИНН {inn}")

    def write_invite_name(self):
        """
        Запись имени пользователя в инвайт
        :return:
        """
        name = self.receive_message_text.strip()
        i = self.get_invite()
        i.name = name
        i.save()
        self.user_log(f"Пользователь {self.chat_id} указал фамилию {name}")

    def write_invite_surname(self):
        """
        Запись фамилии пользователя в инвайт
        :return:
        """
        surname = self.receive_message_text.strip()
        i = self.get_invite()
        i.surname = surname
        i.save()
        self.user_log(f"Пользователь {self.chat_id} указал фамилию {surname}")

    def approve_invite(self):
        """
        Подтверждение инвайта пользователем
        :return:
        """
        i = self.get_invite()
        i.approve_by_user = True
        i.save(update_fields=['approve_by_user'])

    def refuse_invite(self):
        """
        Отказ в подтверждении инвайта
        :return:
        """
        i = self.get_invite()
        i.refused = True
        i.save(update_fields=['refused'])

    # ###########################################################################

    # ############################# РАБОТА С СООБЩЕНИЯМИ ######################
    @property
    def message_text(self):
        """
        Метод получения текста сообщения для кнопки
        :return: текст сообщения
        """
        text = models.Buttons.get_message(self.button_id)
        return text

    @property
    def receive_message_text(self):
        """
        Текстовое содержимое входящего сообщения
        :return: str
        """
        message = self.update.message.text
        self.user_log("[ IN] " + message)
        return message

    def reply_text(self, *args, **kwargs):
        """
        Отправка сообщения
        :param args: соответствуют собственной функции бота reply_text
        :param kwargs: соответствуют собственной функции бота reply_text
        :return: None
        """
        if self.has_callback:
            self.update.callback_query.message.reply_text(*args, **kwargs)
        else:
            self.update.message.reply_text(*args, **kwargs)
        self.user_log("[OUT] " + args[0])

    def drop_message(self):
        """
        Метод удаления текущего сообщения
        :return: None
        """
        if self.has_callback:
            self.update.callback_query.message.delete()
        else:
            self.update.message.delete()
    # ###########################################################################

    # ############################# РАБОТА С АВТОРИЗАЦИЕЙ #####################
    @property
    def pass_is_valid(self):
        """
        Корректность введенного пароля
        :return: boolean
        """
        is_valid = bool(self.receive_message_text == models.BotUser.active_users.get(chat_identifier=self.chat_id).user_code_pass)
        self.user_log("Введен корректный пароль") if is_valid else self.user_log("Введен некорректный пароль")
        return is_valid

    @property
    def is_authorized(self):
        """
        Проверка состояния авторизации потльзователя
        :return: Boolean
        """
        return bool(self.context.user_data.get("is_authorized", False))

    def login_user(self):
        """
        Метод авторизации пользователя
        :return: None
        """
        self.user_log("Пользователь успешно авторизован")
        self.context.user_data["is_authorized"] = True

    def unlogin_user(self):
        """
        Метод разлогинивания пользователя
        :return: None
        """
        self.user_log("Пользователь разлогинен")
        self.context.user_data["is_authorized"] = False
    # ###########################################################################

    # ############################# РАБОТА С МЕНЮ ##############################
    @property
    def has_submenu(self):
        """
        Проверка существования подменю для кнопки
        :return: boolean
        """
        has_s = models.Buttons.has_submenu(self.button_id)
        if not has_s:
            self.user_log(f"Подменю для кнопки {self.button_id} отсутствует")
        return has_s

    @property
    def has_markup(self):
        """
        Проверка существования reply_markup для кнопки
        :return: boolean
        """
        has_s = bool(self.reply_markup.inline_keyboard)
        if not has_s:
            self.user_log(f"reply_markup для кнопки {self.button_id} отсутствует")
        return has_s

    def __main_menu_getter(self):
        return self.context.user_data.get("main_menu", True)

    def __main_menu_setter(self, value):
        self.context.user_data["main_menu"] = value

    main_menu = property(__main_menu_getter, __main_menu_setter, None, "Указатель, текущее меню - главное")

    @property
    def reply_markup(self):
        """
        Формирование меню для кнопки
        :return: reply_markup для меню
        """
        if self.main_menu:
            self.user_log("Главное меню")
            reply_markup = models.Buttons.get_main_menu(self.chat_id)
        else:
            reply_markup = models.Buttons.get_inline_menu(self.button_id)
            self.user_log("Вторичное меню кнопки " + str(self.button_id))
        return reply_markup

    @property
    def user_subscriptions_markup(self):
        """
        Метод получения меню изменения подписок
        :return: InlineKeyboardMarkup object
        """
        return models.BotUser.get_subscriptions_markup(self.chat_id)

    def fix_button_press(self):
        models.Buttons.fix_button_press(self.button_id, self.chat_id)

    def check_timeout_execute_permission(self):
        return models.Buttons.check_timeout_execute_permission(self.button_id, self.chat_id)

    # ###########################################################################

    # ############################# ПРОВЕРКИ НАЖАТИЙ КНОПОК ####################
    @property
    def standard_inline_button_is_pressed(self):
        """
        Проверка нажатия обычной кнопки инлайн-меню
        :return: Boolean
        """
        return bool(self.button_id)

    @property
    def subscription_change_button_is_pressed(self):
        """
        Проверка нажатия кнопки изменения подписки
        :return: Boolean
        """
        return bool(self.subscription_id)

    @property
    def subscription_info_button_is_pressed(self):
        """
        Проверка нажатия кнопки информации о подписке
        :return: Boolean
        """
        return bool(self.subscription_info_id)

    @property
    def go_to_subscriptions_button_is_pressed(self):
        """
        Проверка нажатия кнопки перехода к подпискам
        :return: Boolean
        """
        return bool(self.callback_data.get("subscriptions_button", False))

    @property
    def subscription_exit_button_is_pressed(self):
        """
        Проверка нажатия кнопки выхода из меню редактирования подписок
        :return: Boolean
        """
        return bool(self.callback_data.get("exit_subscriptions_button", False))

    @property
    def info_button_is_pressed(self):
        """
        Проверка нажатия кнопки информации о боте
        :return: Boolean
        """
        return bool(self.callback_data.get("action_info", False))
    # ###########################################################################

    # ############################# РАБОТА С ПОДПИСКАМИ #########################
    def change_user_subscription_activity(self):
        """
        Изменение статуса подписки пользователя
        :return:
        """
        if models.BotUser.subscription_id_active_for_user(self.chat_id, self.subscription_id):
            self.user_log(f"Отключена подписка {self.subscription_id}")
            models.BotUser.off_user_subscription(self.chat_id, self.subscription_id)
        else:
            self.user_log(f"Включена подписка {self.subscription_id}")
            models.BotUser.on_user_subscription(self.chat_id, self.subscription_id)

    @property
    def subscription_info(self):
        """
        Информация о подписке
        :return: информация о подписке (str)
        """
        return models.BotUser.get_subscription_info(self.chat_id, self.subscription_info_id)
    # ###########################################################################

    # ############################# ЛОГИРОВАНИЕ #################################
    def user_log(self, message):
        """
        Метод логирования с префиксом chat_id
        :param message: логируемое сообщение
        :return: None
        """
        l.logger.info(f"[{self.chat_id}] {message}")
    # ###########################################################################

    def make_action(self, action, message):
        self.user_log(f'Вызов экшена "{action}"')
        r.RabbitQueueButtonActions().send(self.button_id, self.chat_id, action.id, message)

    @staticmethod
    def get_action(action_id):
        return models.Actions.get_action(action_id)


class ActionManager:
    def __init__(self, action_id, chat_id, user_message):
        self.bot = b.bot
        self.action_id = action_id
        self.chat_id = chat_id
        self.user_message = user_message

    def _user_log(self, message):
        l.logger.info(f"[{self.chat_id}] Отправлено экшеном: {message}")

    def send_message(self, message, html=False):
        """
        Отправка текстового сообщения
        :param message: текст сообщения
        :param html: если True, парсить message как html, по умолчанию False
        :return: None
        """
        self.bot.send_message(chat_id=self.chat_id, text=str(message), parse_mode=ParseMode.HTML if html else None)
        self._user_log(str(message))

    def send_photo(self, file):
        """
        Отправка фото в чат
        :param file: файл фото
        :return: None
        """
        self.bot.send_photo(chat_id=self.chat_id, photo=open(file, 'rb'))
        self._user_log(f"фото {file}")

    def send_file(self, file):
        """
        Отправка документа в чат
        :param file: файл
        :return: None
        """
        self.bot.send_document(chat_id=self.chat_id, document=open(file, 'rb'))
        self._user_log(f"файл {file}")

    @property
    def user(self):
        """
        Объект пользователя
        Методы и свойства модели BotUser
        :return:
        """
        return list(models.BotUser.get_user(self.chat_id)).pop()

    @property
    def connection_1(self):
        """
        Подключение к БД
        :return:
        """
        return mysql.MysqlConnection(settings.MYSQL_CONNECTIONS.CONNECT_1)



