import matplotlib.pyplot as plt
from itertools import cycle
from os.path import normpath
from PIL import Image
from operator import itemgetter
from django.conf import settings


logo = normpath(settings.GRAPHIC_LOGO)


def create_data_from_dict(dict_for_parsing):
    values = []
    labels = []
    for data in dict_for_parsing:
        values.append(data['cnt'])
        labels.append(data['status'])
    return values, labels


def _autolabel(ax, rects, labels=None, height_factor=1.01):
    for i, rect in enumerate(rects):
        height = rect.get_height()
        if labels is not None:
            try:
                label = labels[i]
            except (TypeError, KeyError):
                label = ' '
        else:
            label = '%d' % int(height)
        ax.text(rect.get_x() + rect.get_width() / 2., height_factor * height,
                '{}'.format(label),
                ha='center', va='bottom')


def _sorting(values, labels, reverse):
    # обратная сортировка, к примеру для pie
    result = sorted({k: v for k, v in zip(values, labels)}.items(), key=itemgetter(0), reverse=reverse)
    values = [x for x, _ in result]
    labels = [x for _, x in result]
    return values, labels


class Bar:
    """
    Класс вертикального бара
    """

    def __init__(self, p, grid, multibar):
        self.colors = cycle(['blue', 'green', 'red', 'grey', 'black', 'yellow'])
        self.plt = p
        self.multibar = multibar
        self.grid = grid
        self.ax = plt.axes()

        self.show_bar_values = False
        self.show_legend = False
        self.bar_name = ''
        self.values_y = []
        self.values_x = []
        self.data_names = []

    def add_bar(self, values, **kwargs):
        """Метод добавления нового бара"""
        self.data_names = kwargs.get('data_names', self.data_names)
        self.bar_name = kwargs.get('bar_name', self.bar_name)
        self.show_bar_values = kwargs.get('show_bar_values', self.show_bar_values)
        self.show_legend = True if self.bar_name or self.show_legend else False
        self.values_y = values

        # подбор параметров оси X
        if self.multibar:
            if self.values_x:
                self.values_x = [x + 1 for x in self.values_x]
            else:
                self.values_x = kwargs.get('values_x', [x * 10 for x in range(1, len(self.values_y) + 1)])
        else:
            self.values_x = kwargs.get('values_x', list(range(1, len(self.values_y) + 1)))

        # прописываем наименования оси X
        if self.data_names:
            self.plt.xticks(self.values_x, self.data_names)

        # рисуем горизонтальный грид
        if self.grid:
            self.ax.yaxis.grid(True, zorder=1)

        # добавляем бар
        try:
            self.plt.bar(self.values_x,
                         self.values_y,
                         color=next(self.colors),
                         alpha=0.7,
                         label=self.bar_name)
        except Exception as e:
            print(e)

        if self.show_legend:
            self.plt.legend(loc='upper right')

        if self.show_bar_values:
            _autolabel(self.ax, self.ax.patches, height_factor=1.01)

    def show(self):
        self.plt.show()

    @staticmethod
    def save(file, watermark=False):
        if not file.endswith('.png'):
            file + '.png'
        plt.savefig(file, bbox_inches='tight')
        if watermark:
            img = Image.open(file)
            logo_f = Image.open(logo)
            img.paste(logo_f, (50, 50), logo_f)
            img.save(file)


class Pie:
    def __init__(self, p):
        self.plt = p
        self.explode = 0
        self.values = []
        self.names = []
        self.show_legend = True
        self.show_names = False
        self.sorts = True
        self.sort_reverse = False

    def add_pie(self, values, names, **kwargs):
        self.explode = kwargs.get('explode', self.explode)
        self.show_legend = kwargs.get('show_legend', self.show_legend)
        self.show_names = kwargs.get('show_names', self.show_names)
        self.sorts = kwargs.get('sorts', True)
        self.sort_reverse = kwargs.get('sort_reverse', True)
        self.names = names
        self.values = values

        # сортируем, если указано
        if self.sorts:
            self.values, self.names = _sorting(self.values, self.names, self.sort_reverse)

        try:
            if self.explode:
                explode = [0 for _ in range(len(self.values))]
                explode[self.explode - 1] = 0.15
            else:
                explode = None
        except:
            explode = None

        try:
            self.plt.pie(
                self.values,
                labels=self.names if self.show_names else None,
                autopct='%1.1f%%',
                # radius=1.1,
                explode=explode,
                shadow=True,
                startangle=135)
        except Exception as e:
            print(e)

        if self.show_legend:
            plt.legend(loc='upper right', labels=self.names)

    def show(self):
        self.plt.show()

    @staticmethod
    def save(file, watermark=False):
        if not file.endswith('.png'):
            file + '.png'
        plt.savefig(file, bbox_inches='tight')
        if watermark:
            img = Image.open(file)
            logo_f = Image.open(logo)
            img.paste(logo_f, (50, 50), logo_f)
            img.save(file)


class Hist:
    def __init__(self, p, grid):
        self.plt = p
        self.grid = grid
        self.show_legend = False
        self.values = []
        self.names = []
        self.length = None
        self.show_names = False
        self.auto_length = False
        self.show_bar_values = False

        self.ax = plt.axes()

    def add_hist(self, values, **kwargs):
        self.values = values
        self.names = kwargs.get('names', self.names)
        self.length = kwargs.get('length', self.length)
        self.auto_length = kwargs.get('auto_length', self.auto_length)
        self.show_bar_values = kwargs.get('show_bar_values', self.show_bar_values)

        # рисуем горизонтальный грид
        if self.grid:
            self.ax.yaxis.grid(True, zorder=1)

        bins = (max(self.values) * 2) if len(self.values) == 1 and self.auto_length else None

        try:
            plt.hist(self.values,
                     bins=bins,
                     label=self.names if self.names else None,
                     alpha=0.7)
        except Exception as e:
            print(e)

        if self.show_bar_values:
            _autolabel(self.ax, self.ax.patches, height_factor=1.01)

        if self.names:
            self.plt.legend(loc='upper right', labels=self.names)

    def show(self):
        self.plt.show()

    @staticmethod
    def save(file, watermark=False):
        if not file.endswith('.png'):
            file + '.png'
        plt.savefig(file, bbox_inches='tight')
        if watermark:
            img = Image.open(file)
            logo_f = Image.open(logo)
            img.paste(logo_f, (50, 50), logo_f)
            img.save(file)


class StatisticImage:
    """Класс изображения с отображением статистики
    :keyword
    title:
        заголовок изображения
    grid:
        отображение грида (default: False)
    watermark:
        отображение watermark (default: False)
    """

    def __init__(self, **kwargs):
        self.title = kwargs.get('title', None)
        self.grid = kwargs.get('grid', False)

        self.plt = plt
        self.plt.figure(figsize=(15, 15))

        self.plt.title(self.title) if self.title else None

    def create_bar_plot(self, **kwargs):
        """Метод создания холста баров
        :keyword
        multibar:
            отображение нескольких баров на одном холсте
        :return
            class.Bar
        """
        multibar = kwargs.get('multibar', False)
        return Bar(self.plt, self.grid, multibar)

    def create_hist_plot(self):
        return Hist(self.plt, self.grid)

    def create_pie_plot(self):
        return Pie(self.plt)
