guarantee_price_query_template = f"""SELECT 
CASE    
WHEN l.withoutPrice = 0 AND l.isEnsure IN (0,2) THEN l.garantPrice
WHEN l.withoutPrice = 0 AND l.isEnsure in (1,3) THEN l.ensurePrice
WHEN l.withoutPrice = 1 THEN 10000
END AS quarantee_price
FROM %(purchase)s r
JOIN %(lots)s l 
ON l.requestId = r.id
WHERE r.actualId IS NULL
AND r.requestNumber = '%(notification_number)s'
AND l.numberLot = '%(lot_number)s'
AND l.statusId NOT IN (2)
;"""

