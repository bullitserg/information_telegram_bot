from abc import ABC, abstractmethod

import bot.scripts.components.module_223.constants.procedure_type as t
import bot.scripts.components.module_223.mapper as m
import bot.scripts.components.module_223.templates.query as q_t


class AbstractProcedure(ABC):

    guarantee_price_query_template = q_t.guarantee_price_query_template
    default_lot_number = 1

    def get_default_lot_number(self):
        """
        Метод получения номера лота по умолчанию
        :return:
        """
        return self.default_lot_number

    def __init__(self, notification_number, *args, **kwargs):
        self.notification_number = notification_number
        self.lot_number = kwargs.get("lot_number", self.get_default_lot_number())
        self.procedure_mapper = m.procedure[self.procedure_type]

    @property
    @abstractmethod
    def procedure_type(self):
        pass

    @property
    def guarantee_price_query(self):
        """
        Свойство: запрос для получения данных гарантийной цены
        :return: запрос str
        """
        subst = self.procedure_mapper["tableName"]
        subst.update({"notification_number": self.notification_number, "lot_number": self.lot_number})
        return self.guarantee_price_query_template % subst


class QProcedureClass(AbstractProcedure):
    procedure_type = t.Q

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class QRProcedureClass(AbstractProcedure):
    procedure_type = t.QR

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class QRSProcedureClass(AbstractProcedure):
    procedure_type = t.QRS

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class QNTProcedureClass(AbstractProcedure):
    procedure_type = t.QNT

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class RProcedureClass(AbstractProcedure):
    procedure_type = t.R

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class RTProcedureClass(AbstractProcedure):
    procedure_type = t.RT

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class KRTProcedureClass(AbstractProcedure):
    procedure_type = t.KRT

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class QRTProcedureClass(AbstractProcedure):
    procedure_type = t.QRT

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class AProcedureClass(AbstractProcedure):
    procedure_type = t.A

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class KProcedureClass(AbstractProcedure):
    procedure_type = t.K

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ZProcedureClass(AbstractProcedure):
    procedure_type = t.Z

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ARProcedureClass(AbstractProcedure):
    procedure_type = t.AR

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class PZProcedureClass(AbstractProcedure):
    procedure_type = t.PZ

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class PProcedureClass(AbstractProcedure):
    procedure_type = t.P

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class OAProcedureClass(AbstractProcedure):
    procedure_type = t.OA

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class CProcedureClass(AbstractProcedure):
    procedure_type = t.C

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class A2ProcedureClass(AbstractProcedure):
    procedure_type = t.A2

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class ARTProcedureClass(AbstractProcedure):
    procedure_type = t.ART

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

