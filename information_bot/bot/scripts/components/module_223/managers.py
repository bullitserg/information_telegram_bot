import re
import bot.scripts.components.module_223.exceptions as e
import bot.scripts.components.module_223.entity.procedure as p


class EntityManager:
    def __init__(self):
        self.procedure_number_pattern = "([A-Z0-9]+)-([0-9]+)(?:/([0-9]+))*"
        self.procedure_class_postfix = "ProcedureClass"
        self.procedure_obj = None

    def _procedure_number_is_valid(self, procedure_number):
        """
        Проверка корректности введенного номера процедуры
        :param procedure_number:
        :return: bool
        """
        return bool(re.fullmatch(self.procedure_number_pattern, procedure_number))

    def get_procedure(self, procedure_number):
        """
        Метод получения экземпляра процедуры
        :param procedure_number: полный номер процедуры
        :return: экземпляр класса _ProcedureClass
        """
        procedure_number = procedure_number.strip()

        if not self._procedure_number_is_valid(procedure_number):
            raise e.IncorrectProcedureNumberException

        procedure_type, notification_number, lot_number = re.findall(self.procedure_number_pattern, procedure_number)[0]

        try:
            procedure_class = getattr(p, str(procedure_type) + self.procedure_class_postfix)
        except AttributeError:
            raise e.IncorrectProcedureTypeException

        if lot_number:
            self.procedure_obj = procedure_class(notification_number, lot_number=lot_number)
        else:
            self.procedure_obj = procedure_class(notification_number)

        return self.procedure_obj




