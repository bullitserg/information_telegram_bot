import bot.scripts.components.module_223.constants.procedure_type as t

procedure = {
    t.Q: {
        'tableName': {
            'purchase': 'quotationRequest',
            'lots': 'quotationLots',
            'requestDoc': 'quotationRequestDoc',
            'requestDocLot': None,
            'lotItems': 'quotationLotItems',
            'lotOffers': 'quotationOffers'}
    },

    t.QR: {
        'tableName': {
            'purchase': 'qr_quotationRequest',
            'lots': 'qr_quotationLots',
            'requestDoc': 'qr_quotationDocRequest',
            'requestDocLot': 'qr_quotationDocLot',
            'lotItems': 'qr_quotationLotItems',
            'lotOffers': 'qr_quotationOffers'
        },
    },

    t.QRS: {
        'tableName': {
            'purchase': 't223_request',
            'lots': 't223_lot',
            'requestDoc': 't223_documentRequest',
            'requestDocLot': 't223_documentLot',
            'lotItems': 't223_lotItem',
            'lotOffers': 't223_offer',
        },
    },

    t.QNT: {
        'tableName': {
            'purchase': 'qnt_quotationRequest',
            'lots': 'qnt_quotationLots',
            'requestDoc': 'qnt_quotationDocRequest',
            'requestDocLot': 'qnt_quotationDocLot',
            'lotItems': 'qnt_quotationLotItems',
            'lotOffers': 'qnt_quotationOffers',
        },
    },

    t.R: {
        'tableName': {
            'purchase': 'profferRequest',
            'lots': 'profferLots',
            'requestDoc': 'profferDocRequest',
            'requestDocLot': 'profferDocLot',
            'lotItems': 'profferLotItems',
            'lotOffers': 'profferOffers',
        },
    },

    t.RT: {
        'tableName': {
            'purchase': 'profferRtRequest',
            'lots': 'profferRtLots',
            'requestDoc': 'profferRtDocRequest',
            'requestDocLot': 'profferRtDocLot',
            'lotItems': 'profferRtLotItems',
            'lotOffers': 'profferRtOffers',
        },
    },

    t.KRT: {
        'tableName': {
            'purchase': 'contestRtRequest',
            'lots': 'contestRtLots',
            'requestDoc': 'contestRtDocRequest',
            'requestDocLot': 'contestRtDocLot',
            'lotItems': 'contestRtLotItems',
            'lotOffers': 'contestRtOffers',
        },
    },

    t.QRT: {
        'tableName': {
            'purchase': 'quotationRtRequest',
            'lots': 'quotationRtLots',
            'requestDoc': 'quotationRtDocRequest',
            'requestDocLot': 'quotationRtDocLot',
            'lotItems': 'quotationRtLotItems',
            'lotOffers': 'quotationRtOffers',
        },
    },

    t.A: {
        'tableName': {
            'purchase': 'auctionRequests',
            'lots': 'auctionLots',
            'requestDoc': 'auctionRequestDocuments',
            'requestDocLot': 'auctionDocLot',
            'lotItems': 'auctionLotItems',
            'lotOffers': 'auctionOffers',
        },
    },

    t.K: {
        'tableName': {
            'purchase': 'contestRequest',
            'lots': 'contestLots',
            'requestDoc': 'contestDocRequest',
            'requestDocLot': 'contestDocLot',
            'lotItems': 'contestLotItems',
            'lotOffers': 'contestOffers',
        },
    },

    t.Z: {
        'tableName': {
            'purchase': 'priceRequest',
            'lots': 'priceLot',
            'requestDoc': 'priceRequestDocuments',
            'requestDocLot': 'priceLotDocuments',
            'lotItems': 'priceLotItems',
            'lotOffers': 'priceOffer',
        },
    },

    t.AR: {
        'tableName': {
            'purchase': 'raiseAuctionRequest',
            'lots': 'raiseAuctionLot',
            'requestDoc': 'raiseAuctionRequestDocuments',
            'requestDocLot': 'raiseAuctionLotDocuments',
            'lotItems': 'raiseAuctionLotItems',
            'lotOffers': 'raiseAuctionOffer',
        },
    },

    t.PZ: {
        'tableName': {
            'purchase': 'positionalRequest',
            'lots': 'positionalLots',
            'requestDoc': 'positionalDocRequest',
            'requestDocLot': 'positionalDocLot',
            'lotItems': 'positionalLotItems',
            'lotOffers': 'positionalOffers',
        },
    },

    t.P: {
        'tableName': {
            'purchase': 'mlpdoRequests',
            'lots': 'mlpdoLots',
            'requestDoc': 'mlpdoRequestDocuments',
            'requestDocLot': None,
            'lotItems': 'mlpdoLotItems',
            'lotOffers': 'mlpdoOffers',
        },
    },

    t.OA: {
        'tableName': {
            'purchase': 'auctionISRequest',
            'lots': 'auctionISLot',
            'requestDoc': 'auctionISRequestDocuments',
            'requestDocLot': 'auctionISLotDocuments',
            'lotItems': 'auctionISLotItems',
            'lotOffers': 'auctionISOffer',
        },
    },

    t.C: {
        'tableName': {
            'purchase': 'cezRequest',
            'lots': 'cezLot',
            'requestDoc': 'cezRequestDocuments',
            'requestDocLot': 'cezLotDocuments',
            'lotItems': 'cezLotItems',
            'lotOffers': 'cezOffers',
        },
    },

    t.A2: {
        'tableName': {
            'purchase': 'a2_auctionRequest',
            'lots': 'a2_auctionLots',
            'requestDoc': 'a2_auctionDocRequest',
            'requestDocLot': 'a2_auctionDocLot',
            'lotItems': 'a2_auctionLotItems',
            'lotOffers': 'a2_auctionOffers',
        },
    },

    t.ART: {
        'tableName': {
            'purchase': 'auctionRtRequest',
            'lots': 'auctionRtLot',
            'requestDoc': 'auctionRtRequestDocuments',
            'requestDocLot': 'auctionRtLotDocuments',
            'lotItems': 'auctionRtLotItems',
            'lotOffers': 'auctionRtOffer',
        },
    },
}






