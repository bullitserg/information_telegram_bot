import bot.models as m
import bot.models as models
import bot.scripts.components.bot as bot
import bot.scripts.components.email as email
from django.utils import timezone


def active_subscriptions_changed(sender, **kwargs):
    # для каждого пользователя и подписки при добавлении ее пользователю
    # создаем новую запись в UserSubscriptionsAdditionalData
    if kwargs["action"] == "pre_add":
        for pk in kwargs["pk_set"]:
            # проверяем наличие записи
            check_record_exists = bool(
                m.UserSubscriptionsAdditionalData.objects.filter(
                    bot_user=kwargs["instance"],
                    subscription=m.Subscriptions.active_subscription_objects.get(pk=pk)))

            # если ее нет, то создаем
            if not check_record_exists:
                additional_info = m.UserSubscriptionsAdditionalData(
                    bot_user=kwargs["instance"],
                    subscription=m.Subscriptions.active_subscription_objects.get(pk=pk),
                    start_datetime=timezone.now())
                additional_info.save()


def invite_changed(sender, **kwargs):
    def send_invite_user_approve_to_manager(kw):
        """
        Отправка менеджеру уведомления о том, что пользователь заапрувил инвайт
        :param kw:
        :return:
        """
        email.send_invite_user_approve_to_manager(kw["instance"])

    def create_user_by_invite(kw):
        """
        Создание пользователя по инвайту
        :param kw:
        :return:
        """
        invite = kw["instance"]
        u = models.BotUser(name=invite.name,
                           surname=invite.surname,
                           email=invite.email,
                           chat_identifier=invite.chat_identifier,
                           user_code_pass=invite.user_code_pass,
                           organization_inn=invite.organization_inn)

        u.save()

        invite.user_created = True
        invite.save()

        # подключаем группы по умолчанию
        for group in models.UserGroup.get_default_groups():
            u.user_groups.add(group)

        u.save()

        email.send_invite_manager_approve_to_user(kw["instance"])
        bot.bot.send_message(chat_id=invite.chat_identifier, text="Поздравляю! Вы успешно подключены. Для авторизации введите четырехзначный код, полученный ранее")

    if kwargs["update_fields"]:
        if ("approve_by_user" in kwargs["update_fields"]) and \
                kwargs["instance"].approve_by_user \
                and not kwargs["instance"].user_created:
            send_invite_user_approve_to_manager(kwargs)

        if ("approve_by_manager" in kwargs["update_fields"]) \
                and kwargs["instance"].approve_by_manager \
                and kwargs["instance"].approve_by_user \
                and not kwargs["instance"].user_created:
            create_user_by_invite(kwargs)




