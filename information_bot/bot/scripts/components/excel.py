from xlwt import *
from decimal import *
from datetime import datetime


class NoDataException(Exception):
    pass


class Excel:
    def __init__(self):
        global wb, styleHeader, styleGeneral, styleNumeral

        # описываем стили
        styleHeader = easyxf('''font: bold 0;
                        border: top_color 23, bottom_color 23, right_color 23, left_color 23,
                                top thin, right thin, bottom thin, left thin;
                        font: color black;
                        pattern: pattern solid, fore_color 22; align: horiz center, vertical center;
                        align: wrap yes;''')
        styleGeneral = easyxf('''font: bold 0;
                        border: top_color 22, bottom_color 22, right_color 22, left_color 22,
                                top thin, right thin, bottom thin, left thin;
                        font: color black;
                        pattern: pattern solid, fore_color white; align: horiz left;
                        align: wrap yes;
                        ''')
        styleNumeral = easyxf('''font: bold 0;
                        border: top_color 22, bottom_color 22, right_color 22, left_color 22,
                                top thin, right thin, bottom thin, left thin;
                        font: color black;
                        pattern: pattern solid, fore_color white; align: horiz left;
                        align: wrap yes;
                        ''', num_format_str='#,##0.00')

        self.file_link = None

        wb = Workbook()

    @staticmethod
    def create_list(sheet_name='Лист'):
        """
        Метод добавления листа в книгу
        :param sheet_name: название листа
        :return: excel_list
        """
        return ExcelList(sheet_name)

    def save_file(self, file_link='Default'):
        """
        Метод сохранения файла
        :param file_link: путь до файла
        :return: путь до файла
        """
        self.file_link = file_link

        if not self.file_link.endswith('.xls'):  # проверяем имя, если расширение не указано - дописываем
            self.file_link += '.xls'

        wb.save(self.file_link)  # сохраняем файл
        return self.file_link


class ExcelList:

    def __new__(cls, excel_list_name):
        obj = super(ExcelList, cls).__new__(cls)
        return obj

    def __init__(self, excel_list_name):
        self.excel_list_name = excel_list_name
        self.need_numeral = ()  # переменная для установки стиля styleNumeral
        self.wb_sheet = wb.add_sheet(self.excel_list_name)
        self.column_for_numerate = -1
        self.col_count = None
        self.col_number_width = 0
        self.col_count_width = None
        self.data_list = None

    def write_data_from_iter(self, data_list, top_line=False):
        """
        Заполнение листа данными
        Наименования берутся из первого набора данных
        Если требуется указать названия столбцов вручную, то они передаются как аргументы через запятую

        Автоматически интерпретируется:
        Символ '№' в заголовке - вместо содержимого указанного столбца пишется нумерация, столбцу выставляется стандартная ширина
        Числа с дробной частью - представляются в формате 'xxx xxx xxx.xx'
        Даты - представляются в формате '%Y-%m-%d %H:%M:%S'
        Ccылки (начинаются с http) - добавляется гиперссылка

        :param data_list: набор данных для записи
        :param top_line: набор заголовков
        :return:
        """
        self.data_list = list(data_list)

        if top_line:  # если не установлен topLine, то берем заголовки из набора данных
            self.data_list.insert(0, top_line)

            # определим колонку, в которой надо расположить нумерацию
            if '№' in top_line:
                self.column_for_numerate = top_line.index('№')
                # и выставим для этой колонки стандартную ширину
                self.wb_sheet.col(self.column_for_numerate).width = 1400

        self.col_count = len(self.data_list[0])  # определяем количество столбцов
        row_number = 0  # начинаем обработку данных

        def is_hyper_link(value):
            """
            Проверка на гиперссылку
            :param value: строка для проверки
            :return: Bool
            """
            return str(value).startswith('http')

        for get_data in self.data_list:
            col_number = 0

            while col_number < self.col_count:  # работа со строкой
                write_data = get_data[col_number]  # это будем писать по умолчанию
                if row_number:
                    # если не заголовок, выставляем обычный стиль
                    use_style = styleGeneral

                    if col_number == self.column_for_numerate:
                        write_data = row_number

                    # а может надо отобразить как число (указано через функцию setNumeral)?
                    elif col_number + 1 in self.need_numeral:
                        use_style = styleNumeral
                        write_data = Decimal(write_data)

                    # а может оно и так decimal, значит отформатировать не повредит
                    elif isinstance(write_data, Decimal):
                        use_style = styleNumeral

                    # а если дата, то надо преобразовать ее в нормальный вид
                    elif isinstance(write_data, datetime):
                        write_data = write_data.__format__('%Y-%m-%d %H:%M:%S')

                    # если гиперссылка, то отображаем как ссылку
                    elif is_hyper_link(write_data):
                        write_data = Formula('HYPERLINK("' + write_data + '")')
                else:
                    use_style = styleHeader  # стиль для заголовка

                self.wb_sheet.write(row_number,  # пишем данные в ячейку
                                    col_number,
                                    write_data,
                                    use_style)
                col_number += 1
            row_number += 1

    def set_column_width(self, *args):
        """
        Метод выставления ширины столбца
        :param args: ширина каждого столбца через запятую
        :return: None
        """
        self.col_count_width = len(args)
        for self.column in args:
            while self.col_number_width < self.col_count_width:
                self.wb_sheet.col(self.col_number_width).width = round(args[self.col_number_width] * 37)
                self.col_number_width += 1

    def set_default_column_width(self, width):
        """
        Метод изменения ширины столбцов по умолчанию
        Если в колонке расположен номер, то ширина не меняется
        :param width: ширина столбца
        :return: None
        """
        self.col_number_width = 0

        if not self.data_list:
            raise NoDataException("Перед использованием setDefaultColumnWidth необходимо наполнить лист данными")

        self.col_count_width = len(self.data_list[0])  # количество столбцов
        for self.column in range(self.col_count):
            while self.col_number_width < self.col_count_width:
                # меняем ширину, только если в колонке не номер
                if not self.column_for_numerate == self.col_number_width:
                    self.wb_sheet.col(self.col_number_width).width = round(width * 37)
                self.col_number_width += 1

    def set_numeral(self, *args):
        """
        Метод для отображения содержимого столбца в представлении типа 55 554,50
        Применять метод необходимо до записи данных в ячейки
        Необязательно применять к столбцам, имеющим дробную часть (то есть относящимся к Decimal), к ним представление применится автоматически
        В качестве аргументов через запятую необходимо указать столбцы, к которым применить отображение
        К строке заголовка метод не применяется
        :param args: номера столбцов через запятую
        :return: None
        """
        self.need_numeral = args



