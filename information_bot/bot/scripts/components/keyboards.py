from django.conf import settings
from telegram import ReplyKeyboardMarkup

custom_menu_standard_items = [['/menu', '/other', '/exit'], ]

# KEYBOARDS
CUSTOM_MENU_STANDARD = ReplyKeyboardMarkup(custom_menu_standard_items, resize_keyboard=True)


def lining_keyboard(array, button_in_line=settings.DEFAULT_BUTTONS_IN_KEYBOARD_LINE):
    """
    Функция для разбития клавиатуры на строки
    :param array: массив кнопок
    :param button_in_line: количество кнопок в ряду
    :return: разбитый массив кнопок
    """
    final_array = []
    while True:
        tmp_array = []
        for _ in range(button_in_line):
            try:
                tmp_array.append(array.pop(0))
            except IndexError:
                if tmp_array:
                    final_array.append(tmp_array)
                return final_array
        final_array.append(tmp_array)




