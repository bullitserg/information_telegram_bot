import bot.models as m
import bot.scripts.components.rabbit as r


# функция для создания функции джобы
def job_creator(*args, **kwargs):
    def func(context):
        job_code = str(context.job.name).replace("_periodical", "").replace("_daily", "")
        # получаем подписку по ее коду
        job_subscription = m.Subscriptions.objects.get(codename=job_code)
        # получаем все экшены для данной подписки
        job_actions = job_subscription.actions.all()

        # по всем пользователям, подписанным на данную подписку формируем сообщения в кролика
        for user in job_subscription.active_user.all():
            for job_action in job_actions:
                # отправляем в кролика
                r.RabbitQueueSubscription().send(job_code, user.chat_identifier, job_action.pk)
    return func
