class Html(str):
    def __init__(self, string):
        super().__init__()
        self.string = str(string)

    def p(self):
        self.string = "<p>" + self.string + "</p>"
        return Html(self.string)

    def i(self):
        self.string = "<i>" + self.string + "</i>"
        return Html(self.string)

    def b(self):
        self.string = "<b>" + self.string + "</b>"
        return Html(self.string)

    def a(self, url):
        self.string = "<a href='" + str(url) + "'>" + self.string + "</a>"
        return Html(self.string)



