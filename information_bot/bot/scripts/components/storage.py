from os import remove, makedirs
from os.path import join, normpath, exists

from django.conf import settings
from django.core.files.storage import Storage


class BotStorage(Storage):

    def __init__(self, *chat_id):
        self.basedir = normpath(settings.FILE_UPLOAD_TEMP_DIR)
        self.subdir = normpath(join("user_files", str(chat_id[0])[0:3], str(chat_id[0])) if chat_id else "tmp")
        self.dir = normpath(join(self.basedir, self.subdir))
        if not exists(join(self.dir)):
            makedirs(self.dir)

    def path(self, name):
        """
        Returns a local filesystem path where the file can be retrieved using
        Python's built-in open() function. Storage systems that can't be
        accessed using open() should *not* implement this method.
        """
        return join(self.dir, name)

    # The following methods form the public API for storage systems, but with
    # no default implementations. Subclasses must implement *all* of these.

    def delete(self, name):
        """
        Deletes the specified file from the storage system.
        """
        return remove(join(self.dir, name))

    def exists(self, name):
        """
        Returns True if a file referenced by the given name already exists in the
        storage system, or False if the name is available for a new file.
        """
        return exists(join(self.dir, name))

    def listdir(self, path):
        """
        Lists the contents of the specified path, returning a 2-tuple of lists;
        the first item being directories, the second item being files.
        """
        pass

    def size(self, name):
        """
        Returns the total size, in bytes, of the file specified by name.
        """
        pass

    def url(self, name):
        """
        Returns an absolute URL where the file's contents can be accessed
        directly by a Web browser.
        """
        pass

    def accessed_time(self, name):
        """
        Returns the last accessed time (as datetime object) of the file
        specified by name.
        """
        pass

    def created_time(self, name):
        """
        Returns the creation time (as datetime object) of the file
        specified by name.
        """
        pass

    def modified_time(self, name):
        """
        Returns the last modified time (as datetime object) of the file
        specified by name.
        """
        pass
