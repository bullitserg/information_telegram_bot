from django.conf import settings
from django.core.mail import send_mail, mail_managers


def send_invite_user_approve_to_manager(invite_obj):
    mail_managers(f"Пользователь {invite_obj.name} {invite_obj.surname} принял инвайт",
                  f"""Здравствуйте.
Пользователь {invite_obj.name} {invite_obj.surname} организации ИНН {invite_obj.organization_inn} (email {invite_obj.email}) принял инвайт telegram-бота {settings.BOT_IDENTIFIER}.
Необходимо согласовать инвайт.
Спасибо
""",
                  fail_silently=True)


def send_invite_manager_approve_to_user(invite_obj):
    send_mail(f"Подключение к telegram-боту {settings.BOT_IDENTIFIER} согласовано", f"""Здравствуйте.
Вы успешно подключены к telegram-боту компании {settings.BOT_ORGANIZATION_NAME}.
По согласованию с менеджером вам могут быть предоставлены дополнительные возможности.
Для авторизации используйте код {invite_obj.user_code_pass}.
Приятного общения!
""",
              settings.EMAIL_ADDRESS,
              [invite_obj.email, ],
              fail_silently=True)

