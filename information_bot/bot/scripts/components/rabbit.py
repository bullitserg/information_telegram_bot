import json
from abc import ABC, abstractmethod

import bot.scripts.components.logger as l
import pika
from django.conf import settings

credentials = pika.PlainCredentials(username=settings.RABBIT_USER, password=settings.RABBIT_PASSWORD)

rmq_parameters = pika.ConnectionParameters(host=settings.RABBIT_HOST,
                                           port=settings.RABBIT_PORT,
                                           virtual_host=settings.RABBIT_VHOST,
                                           credentials=credentials)

rmq_connection = pika.BlockingConnection(rmq_parameters)
rmq_channel = rmq_connection.channel()
rmq_channel.queue_declare(queue=settings.RABBIT_SUBSCRIPTIONS_QUEUE, durable=True)
rmq_channel.queue_declare(queue=settings.RABBIT_BUTTON_ACTIONS_QUEUE, durable=True)


def stay_alive(*args, **kwargs):
    rmq_connection.process_data_events()


class RabbitQueueBase(ABC):

    exchange = settings.RABBIT_EXCHANGE

    @property
    @abstractmethod
    def queue(self):
        raise NotImplementedError

    @abstractmethod
    def create_dict(self, *args):
        raise NotImplementedError

    @abstractmethod
    def logging(self, *args):
        raise NotImplementedError

    def send(self, *args):
        my_dict = self.create_dict(*args)
        my_json = json.dumps(my_dict)

        rmq_channel.basic_publish(exchange=self.exchange,
                                  routing_key=self.queue,
                                  body=my_json)
        self.logging(*args)
        return my_dict

    def get(self, callback):
        # получаем данные из кролика и передаем в обработчик
        rmq_channel.basic_consume(queue=self.queue,
                                  on_message_callback=callback)

        rmq_channel.start_consuming()


class RabbitQueueSubscription(RabbitQueueBase):
    queue = settings.RABBIT_SUBSCRIPTIONS_QUEUE

    def create_dict(self, job, user_chat_id, action_id):
        return {"job": job, "user_chat_id": user_chat_id, "action_id": action_id}

    def logging(self, job, user_chat_id, action_id):
        l.logger.info(f'Send action "{action_id}" by job "{job}" for user {user_chat_id}')


class RabbitQueueButtonActions(RabbitQueueBase):
    queue = settings.RABBIT_BUTTON_ACTIONS_QUEUE

    def create_dict(self, button_id, user_chat_id, action_id, user_message):
        return {"button_id": button_id, "user_chat_id": user_chat_id,
                "action_id": action_id, "user_message": user_message}

    def logging(self, button_id, user_chat_id, action_id, user_message):
        user_message_loging = (" with message \"" + str(user_message) + "\"") if user_message else ""
        l.logger.info(f'Send action "{action_id}" by button "{button_id}" for user {user_chat_id}{user_message_loging}')





