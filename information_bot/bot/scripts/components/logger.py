import logging

from django.conf import settings

# Enable logging
logging.basicConfig(format=settings.LOG_FORMAT,
                    # level=logging.INFO,
                    level=getattr(logging, settings.LOG_LEVEL, logging.INFO),
                    datefmt=settings.LOG_DATE_FORMAT)

logger = logging.getLogger(settings.BOT_IDENTIFIER)






