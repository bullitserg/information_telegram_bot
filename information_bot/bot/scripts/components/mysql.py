from contextlib import contextmanager as _contextmanager

import mysql.connector


class NonSelectQueryException(Exception):
    pass


class ConnectionNotExistException(Exception):
    pass


class MysqlConnection:
    """Класс для работы с MYSQL"""
    def __init__(self, connection):
        """
        Инициализация подключения
        :param connection: требуемое подключение из settings.MYSQL_CONNECTIONS
        """

        self.connection_attrs = connection
        self._cnx = False
        self._mysql_cur = False

    def __str__(self):
        return str(f"({self.__class__.__name__}:{self.connection_attrs['user']}@{self.connection_attrs['host']}, CONNECTED={bool(self._cnx)})")

    def connect(self):
        """
        Метод подключения к БД
        :return: connection
        """
        # подключаемся
        self._cnx = mysql.connector.connect(**self.connection_attrs)

        return self

    @_contextmanager
    def open(self):
        """
        Метод для использования в контекстном менеджере
        :return:
        """
        # подключаемся
        self.connect()
        # возвращаем подключение
        yield self
        # после всех действий не забываем закрыть
        self.disconnect()

    def execute_query(self, query, *args, as_dict=False):
        """
        Метод выполнения запросов к БД
        :param query: запрос
        :param args: подстановки, указанные в запросе через %s
        :param as_dict: True если требуется возвращать строки словарями (по умолчанию FALSE)
        :return: Результат выборки
        """

        # убираем пробелы из начала запроса
        query = query.lstrip()

        # если не SELECT, то возвращаем ошибку
        if not query.lower().startswith('select'):
            raise NonSelectQueryException("Допустимы только запросы на выборку")

        if not self._cnx:
            raise ConnectionNotExistException("Подключение отсутствует")

        # определяем курсор (для именованных или dictionary требуется определить с dictionary)
        self._mysql_cur = self._cnx.cursor(dictionary=as_dict)

        # выполнение запроса
        if args:
            self._mysql_cur.execute(query, tuple(args))
        else:
            self._mysql_cur.execute(query)

        # получаем данные
        mysql_out = self._mysql_cur.fetchall()

        return tuple(mysql_out)

    def disconnect(self):
        """
        Метод отключения от БД
        :return: None
        """
        try:
            self._mysql_cur.close()
        except:
            pass

        self._cnx = False
        self._mysql_cur = False
