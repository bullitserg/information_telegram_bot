import json
import traceback

import bot.models as m
import bot.scripts.actions.basic as b_handlers
import bot.scripts.components.logger as lg
import bot.scripts.components.rabbit as r


def main():
    def _get_func(func_name):
        exec_function = None
        try:
            exec_function = getattr(b_handlers, func_name)
        except AttributeError:
            lg.logger.error(f"Функция '{func_name}' для экшена не объявлена")

        return exec_function

    def rmq_rabbit_subscriptions_queue_worker(ch, method, properties, body):
        json_string = json.loads(body.decode("utf8"))
        action_id = json_string["action_id"]
        chat_id = json_string["user_chat_id"]

        # получаем наименование функции для указанного экшена и выполняем ее
        exec_function_name = m.Actions.active_actions.get(pk=action_id).exec_function

        exec_function = _get_func(exec_function_name)

        if exec_function:
            lg.logger.info(f"Вызов функции {exec_function_name}")

            # функция всегда определяется через action_id, chat_id, user_message
            try:
                exec_function(action_id, chat_id, None)
            except Exception:
                lg.logger.error(f"Ошибка при вызове функции '{exec_function_name}': {traceback.format_exc()}")

        ch.basic_ack(delivery_tag=method.delivery_tag)

    r.RabbitQueueSubscription().get(rmq_rabbit_subscriptions_queue_worker)


def run():
    main()
