import bot.scripts.components.excel as e
import bot.scripts.components.graphic as g
import bot.scripts.components.managers as m
from bot.scripts.components.module_223.managers import EntityManager
import bot.scripts.components.module_223.exceptions as exceptions
import bot.scripts.components.storage as s
from django.utils import timezone


# функция всегда определяется через action_id, chat_id, user_message
# аналогично определяется ActionManager


def test_function(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    a_m.send_message("I'm sorry Dave I'm afraid I can't do that.")


def test_histogram(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)

    storage = s.BotStorage(chat_id)
    file = storage.path("test_hist.png")

    i = g.StatisticImage(title='Тестовая гистограмма')
    h = i.create_hist_plot()
    h.add_hist([[1, 2, 2, 3, 4], [1, 2, 3, 3, 3, 4], [5, 3, 3]], names=['Распределение 1',
                                                                        'И 2',
                                                                        'А это третье'], auto_length=False)
    h.save(file, watermark=True)

    a_m.send_message("Пример гистограммы")
    a_m.send_photo(file)


def test_pie(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)

    storage = s.BotStorage(chat_id)
    file = storage.path("test_pie.png")

    i = g.StatisticImage(title='Тестовая круговая диаграмма')
    p = i.create_pie_plot()
    p.add_pie([12, 2, 3, 4, 5, 6],
              ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
              explode=1,
              show_names=True)

    p.save(file, watermark=True)

    a_m.send_message("Пример круговой диаграммы")
    a_m.send_photo(file)


def test_bar(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)

    storage = s.BotStorage(chat_id)
    file = storage.path("test_bar.png")

    i = g.StatisticImage(grid=True, title='Тестовая диаграмма')
    b = i.create_bar_plot(multibar=True)
    b.add_bar([10, 11, 112], data_names=['Январь', 'Февраль', 'Март'], bar_name='2018')
    b.add_bar([10, 11, 112], bar_name='2019')
    b.add_bar([12, 65, 14], bar_name='2020')
    b.save(file, watermark=True)

    a_m.send_message("Пример диаграммы")
    a_m.send_photo(file)


def test_excel(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)

    storage = s.BotStorage(chat_id)
    file_link = storage.path("test_excel.xls")

    excel_file = e.Excel()
    excel_list = excel_file.create_list(sheet_name='Страничка')
    excel_list.write_data_from_iter([[1, 2.566, 3, ], [4, "Что нибудь тут", 6, ], [7, 8, 9, ], ],
                                    top_line=["Столбик 1", "Второй", "Третий"])
    excel_list.set_default_column_width(150)
    excel_file.save_file(file_link=file_link)

    a_m.send_message("Пример excel-файла")
    a_m.send_file(file_link)


def test_timeout(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    a_m.send_message("Это сообщение вы можете увидеть не чаще, чем раз в 30 секунд")


def test_interactive(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    a_m.send_message(f"Привет, {user_message}! Рад тебя видеть.")


def test_go_home(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    a_m.send_message("Пора домой!")


def test_period(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    a_m.send_message(f"Время по UTC: {timezone.now().time()}")


def test_from_db(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    with a_m.connection_1.open() as o:
        from_bd = o.execute_query("SELECT 'Это я получил из БД'")[0][0]
    a_m.send_message(from_bd)


def test_bank_guarantee(action_id, chat_id, user_message):
    a_m = m.ActionManager(action_id, chat_id, user_message)
    e_m = EntityManager()
    try:
        procedure_object = e_m.get_procedure(user_message)
    except exceptions.IncorrectProcedureNumberException:
        a_m.send_message("Извините, вы ввели некорректный номер процедуры")
        return
    except exceptions.IncorrectProcedureTypeException:
        a_m.send_message("Извините, вы ввели некорректный тип процедуры")
        return

    with a_m.connection_1.open() as o:
        try:

            # проверить и выпилить
            print(procedure_object.guarantee_price_query)
            return
            #

            guarantee_price_db = o.execute_query(procedure_object.guarantee_price_query)[0][0]
        except IndexError:
            a_m.send_message("Извините, невозможно определить размер гарантии")
            return

    guarantee_price = guarantee_price_db if guarantee_price_db else 0

    a_m.send_message(f"Обеспечение закупки {e_m.procedure_number}: {guarantee_price}р.")

