#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import bot.models as models
import bot.scripts.components.job as j
import bot.scripts.components.rabbit as r
import bot.scripts.components.states as states
import bot.scripts.handlers.basic as basic_handlers
from django.conf import settings
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler, ConversationHandler


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(settings.BOT_TOKEN,
                      base_url=settings.BOT_PROXY if settings.BOT_PROXY else None,
                      use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    converse_handler = ConversationHandler(
        # чтобы начать - достаточно отправить любое сообщение
        entry_points=[MessageHandler(Filters.text, basic_handlers.start),
                      MessageHandler(Filters.command, basic_handlers.start)],

        states={
            states.PASS_CHECKING:       [MessageHandler(Filters.text, basic_handlers.pass_checking),
                                         MessageHandler(Filters.command, basic_handlers.pass_checking),
                                         ],

            states.COMMAND_AWAITING:    [CommandHandler('menu', basic_handlers.main_menu_handler),
                                         CommandHandler('other', basic_handlers.other_handler),
                                         CallbackQueryHandler(basic_handlers.inline_menu_handler),
                                         ],

            states.WAIT_USER_ANSWER:    [CommandHandler('menu', basic_handlers.main_menu_handler),
                                         CommandHandler('other', basic_handlers.other_handler),
                                         MessageHandler(Filters.text, basic_handlers.wait_user_answer),
                                         ],

            states.INVITE_GET_INN:      [MessageHandler(Filters.text, basic_handlers.invite_write_inn),
                                         ],

            states.INVITE_GET_SURNAME:  [MessageHandler(Filters.text, basic_handlers.invite_write_surname),
                                         ],

            states.INVITE_GET_NAME:     [MessageHandler(Filters.text, basic_handlers.invite_write_name),
                                         ],

        },
        fallbacks=[CommandHandler('exit', basic_handlers.exit_bot)]
    )

    # pika не умеет в heartbeat, поэтому поддерживаем соединение, пока бот запущен
    updater.job_queue.run_repeating(r.stay_alive,
                                    interval=60,
                                    first=0,
                                    name="stay_alive_rabbit_periodical")

    # создаем джобы по всем активным подпискам
    subscriptions = models.Subscriptions.active_subscription_objects.all()
    for subscription in subscriptions:
        if subscription.is_periodical:
            updater.job_queue.run_repeating(j.job_creator(subscription.codename),
                                            interval=subscription.interval,
                                            context=subscription,
                                            first=subscription.indent,
                                            name=subscription.codename + "_periodical")
        if subscription.is_daily:
            for exec_time_obj in subscription.subscriptionexecutetime_set.all():
                updater.job_queue.run_daily(j.job_creator(subscription.codename),
                                            exec_time_obj.exec_time,
                                            context=subscription,
                                            days=tuple([i.index for i in subscription.exec_days.all()]),
                                            name=subscription.codename + "_daily")
    dp.add_handler(converse_handler)
    dp.add_error_handler(basic_handlers.error)
    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


def run():
    main()











