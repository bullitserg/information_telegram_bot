import json

import bot.scripts.components.emoji as e
import bot.scripts.components.html as h
import bot.scripts.components.keyboards as keyboards
import bot.scripts.components.logger as l
import bot.scripts.components.managers as m
import bot.scripts.components.states as states
from django.conf import settings
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram import ReplyKeyboardRemove
from telegram.ext import ConversationHandler


def start(update, context):
    d = m.DataManager(update, context)
    if not d.user_is_registered:

        if d.has_invite():
            d.write_invite_chat_id()
            d.reply_text("Добрый день! Вам подтверждена регистрация по инвайту. Для регистрации введите ИНН вашей организации.")
            return states.INVITE_GET_INN

        d.reply_text(f"Извините, вы не зарегистрированы. Если у вас есть инвайт, введите его. Для регистрации обратитесь по адресу {settings.BOT_ADMINISTRATOR_EMAIL} При обращении укажите идентификатор {d.chat_id}",
                     reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END

    if d.has_invite():
        d.write_invite_chat_id()
        d.refuse_invite()
        d.reply_text("Извините, у вас уже есть регистрация. Ваш инвайт аннулирован. Пожалуйста, авторизуйтесь с использованием ранее выданного вам кода авторизации.")
        return ConversationHandler.END

    if d.pass_is_valid:
        d.login_user()

        d.reply_text("Вы успешно авторизованы", reply_markup=keyboards.CUSTOM_MENU_STANDARD)
        return states.COMMAND_AWAITING

    d.reply_text(f"Здравствуйте, {d.user_fio}. Введите ваше кодовое слово")

    return states.PASS_CHECKING


def invite_write_inn(update, context):
    d = m.DataManager(update, context)
    d.write_invite_inn()
    d.reply_text("Спасибо. Укажите ваше имя.")
    return states.INVITE_GET_NAME


def invite_write_name(update, context):
    d = m.DataManager(update, context)
    d.write_invite_name()
    d.reply_text("Спасибо. И для окончания регистрации нам потребуется ваша фамилия.")
    return states.INVITE_GET_SURNAME


def invite_write_surname(update, context):
    d = m.DataManager(update, context)
    d.write_invite_surname()
    d.approve_invite()
    d.reply_text("Спасибо. После подтверждения ваших данных менеджером вы получите доступ ко всем функциям бота. Ваш код авторизации: " +
                 h.Html(d.user_invite.user_code_pass).b() +
                 ". Хорошего дня!",
                 parse_mode=ParseMode.HTML)
    return ConversationHandler.END


def pass_checking(update, context):
    d = m.DataManager(update, context)
    if not d.pass_is_valid:
        d.reply_text("Извините, вы некорректно ввели кодовое слово. Попробуйте еще раз")
        return states.PASS_CHECKING

    d.login_user()
    d.reply_text("Вы успешно авторизованы", reply_markup=keyboards.CUSTOM_MENU_STANDARD)
    return states.COMMAND_AWAITING


def main_menu_handler(update, context):
    d = m.DataManager(update, context)
    d.drop_message()

    # эта проверка необходима, на тот случай, если пользователь авторизован, но в это время его удалили
    # после нажатия на кнопку "/menu" его разлогинит
    if not d.user_is_registered:
        return ConversationHandler.END

    d.main_menu = True
    if d.has_markup:
        d.reply_text(h.Html("Главное меню").b(), parse_mode=ParseMode.HTML, reply_markup=d.reply_markup)
    else:
        d.reply_text("Действия не назначены. Обратитесь к администратору.")

    return states.COMMAND_AWAITING


def exit_bot(update, context):
    d = m.DataManager(update, context)
    del d.last_action_callback
    d.drop_message()
    d.unlogin_user()
    d.reply_text('До свидания! Будем рады видеть Вас снова', reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def other_handler(update, context):
    d = m.DataManager(update, context)
    d.drop_message()

    button_array_for_markup = [[InlineKeyboardButton(e.INFORMATION_SOURCE + " " + "Инфо",
                                                     callback_data=json.dumps({"action_info": True}))], ]

    if d.last_action_callback:
        # если указан колбэк для последней нажатой кнопки, то добавляем кнопку повтора последней команды
        button_array_for_markup.append([InlineKeyboardButton(e.CLOCKWISE_DOWNWARDS_AND_UPWARDS_OPEN_CIRCLE_ARROWS +
                                                             " " + "Повтор последней команды",
                                                             callback_data=d.last_action_callback)])

    inline_keyboard = InlineKeyboardMarkup(button_array_for_markup)

    d.reply_text("Дополнительно:", reply_markup=inline_keyboard)
    return states.COMMAND_AWAITING


def inline_menu_handler(update, context):
    d = m.DataManager(update, context)
    d.main_menu = False
    if d.go_to_subscriptions_button_is_pressed:
        # если нажата кнопка перехода в подписки

        d.last_action_callback = d.callback_data
        d.drop_message()
        d.reply_text(h.Html("Менеджер подписок").b(),
                     parse_mode=ParseMode.HTML,
                     reply_markup=d.user_subscriptions_markup)
        return states.COMMAND_AWAITING

    if d.standard_inline_button_is_pressed:
        # если нажата обычная менюшная кнопка
        is_permitted, second_to_exec = d.check_timeout_execute_permission()
        if not is_permitted:
            d.drop_message()
            d.reply_text(f"Действие кнопки ограничено. Повторное использование возможно через {int(second_to_exec)} секунд")
            return states.COMMAND_AWAITING
        actions = d.actions
        d.last_action_callback = d.callback_data

        if d.actions:
            d.drop_message()
            for action in d.not_interactive_actions:
                d.make_action(action, None)

            for action in d.interactive_actions:
                # обрабатывается только первый интерактивный экшен
                d.action_id = action.id
                d.reply_text(h.Html(d.message_text).b(), parse_mode=ParseMode.HTML, reply_markup=d.reply_markup)
                d.fix_button_press()
                return states.WAIT_USER_ANSWER
            d.fix_button_press()
            return states.COMMAND_AWAITING

        if d.has_submenu:
            d.reply_text(h.Html(d.message_text).b(), parse_mode=ParseMode.HTML, reply_markup=d.reply_markup)
        elif not actions:
            d.reply_text("Функционал для кнопки не назначен. Пожалуйста, Обратитесь в службу поддержки")
        d.fix_button_press()
        d.drop_message()

    if d.subscription_change_button_is_pressed:
        # если нажата кнопка подключения/отключения подписки
        d.change_user_subscription_activity()
        d.drop_message()
        d.reply_text("Менеджер подписок", reply_markup=d.user_subscriptions_markup)

    if d.subscription_info_button_is_pressed:
        # если нажата кнопка получения информации о подписке
        d.drop_message()
        d.reply_text(d.subscription_info, parse_mode=ParseMode.HTML)
        d.reply_text("Менеджер подписок", reply_markup=d.user_subscriptions_markup)

    if d.subscription_exit_button_is_pressed:
        # если нажата кнопка выхода из меню работа с подписками
        d.user_log("Менеджер подписок закрыт")
        d.drop_message()

    if d.info_button_is_pressed:
        # если нажата кнопка получения информации о боте
        d.user_log("Получение информации о боте")
        d.reply_text(f'''{h.Html(settings.BOT_IDENTIFIER).b()}
        Версия: {settings.BOT_VERSION}
        -----------------------------------------
        Контакты для связи с администратором:
        Email: {settings.BOT_ADMINISTRATOR_EMAIL}
        Обязательно укажите ваш ID: {d.chat_id}''',
                     parse_mode=ParseMode.HTML,
                     reply_markup=keyboards.CUSTOM_MENU_STANDARD)
        d.drop_message()
    return states.COMMAND_AWAITING


def wait_user_answer(update, context):
    d = m.DataManager(update, context)
    d.main_menu = False
    action = d.get_action(d.action_id)
    d.make_action(action, d.receive_message_text)
    d.drop_message()
    return states.COMMAND_AWAITING


def error(update, context):
    """Log Errors caused by Updates."""
    l.logger.warning('Update "%s" caused error "%s"', update, context.error)


