import random

import bot.components.email as email
import bot.models as models
from django.db.utils import IntegrityError


class EmailDuplicateError(Exception):
    pass


class Invite:
    def __init__(self, email):
        self.email = email
        self.invite_code = None

    def _generate_invite_code(self):
        self.invite_code = str(random.randint(0, 999999)).rjust(6, "0")
        return self.invite_code

    def _write_invite(self):
        p = models.Invites(email=self.email,
                           invite_code=self.invite_code,
                           user_code_pass=self.invite_code[0:4])
        try:
            p.save(force_insert=True)
        except IntegrityError:
            raise EmailDuplicateError

    def _send_user_invite(self):
        email.send_user_invite(self)

    def create_invite(self):
        self._generate_invite_code()
        try:
            self._write_invite()
        except EmailDuplicateError:
            return False, f"На email {self.email} ранее уже был отправлен инвайт"
        self._send_user_invite()
        return True, None


