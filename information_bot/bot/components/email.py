from django.conf import settings
from django.core.mail import send_mail


def send_user_invite(invite):
    send_mail(f"Подключение к telegram-боту {settings.BOT_IDENTIFIER}", f"""Здравствуйте.
Данное письмо является приглашением к подключению к telegram-боту компании {settings.BOT_ORGANIZATION_NAME}.
Для подключения к боту добавьте в свой список контактов telegram пользователя {settings.BOT_USERNAME}, указав код {invite.invite_code}.
Ответьте на вопросы, которые задаст Вам бот, тогда менеджер подтвердит Вашу заявку и Вы сможете пользоваться ботом.
Приятного общения!
""",
              settings.EMAIL_ADDRESS,
              [invite.email, ],
              fail_silently=True)



