import json

import bot.scripts.components.emoji as e
import bot.scripts.components.html as h
import bot.scripts.components.keyboards as k
import bot.scripts.components.signals as s
import pytz
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import ObjectDoesNotExist
from django.db.models.signals import m2m_changed, post_save
from django.utils import timezone
from telegram import InlineKeyboardButton, InlineKeyboardMarkup


class BotAdministrator(User):

    def __str__(self):
        return self.get_full_name()

    def __repr__(self):
        return self.__str__()

    def get_full_name(self):
        s = (self.last_name if self.last_name else "") + " " + (self.first_name if self.first_name else "")
        return "-" if s == " " else s

    get_full_name.short_description = 'Администратор'

    class Meta(User.Meta):
        proxy = True
        verbose_name = "администратора"
        verbose_name_plural = "Администраторы"


class ActiveActions(models.Manager):
    def get_queryset(self):
        return super(ActiveActions, self).get_queryset().filter(active=True)


class Actions(models.Model):

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    name = models.CharField("Наименование экшена", max_length=100)
    exec_function = models.CharField('Функция для выполнения', max_length=100,
                                     help_text="Наименование функции, которая запускается в экшене")
    active = models.BooleanField("Активный", db_index=True, default=True)
    is_interactive = models.BooleanField("Интерактивный", db_index=True, default=False,
                                         help_text="Запрашивать у пользователя введение сообщения")
    description = models.TextField("Описание экшена", blank=True, null=True)
    objects = models.Manager()
    active_actions = ActiveActions()

    @staticmethod
    def get_action(action_id):
        """
        Получить экшен по его id
        :param action_id:
        :return:
        """
        return Actions.active_actions.get(pk=action_id)

    def get_absolute_url(self):
        return "/bot/actions/" + str(self.id)

    class Meta:
        db_table = 'actions'
        verbose_name = "экшен"
        verbose_name_plural = "Экшены"


class ActiveButtonObjects(models.Manager):
    def get_queryset(self):
        return super(ActiveButtonObjects, self).get_queryset().filter(active=True)


class Buttons(models.Model):
    # кнопки

    def __str__(self):
        return self.text

    def __repr__(self):
        return self.__str__()

    # menu = models.ForeignKey(Menu, on_delete=models.CASCADE, verbose_name="Меню")
    text = models.CharField("Текст кнопки", max_length=50)
    message = models.CharField("Сообщение", max_length=100,
                               help_text="Выводится при нажатии на кнопку меню")
    submenu = models.ManyToManyField('self',
                                     symmetrical=False,
                                     verbose_name="кнопки подменю",
                                     related_name="submenu_buttons",
                                     blank=True)
    actions = models.ManyToManyField(Actions,
                                     symmetrical=False,
                                     verbose_name="действия для кнопки",
                                     related_name="actions_for_button",
                                     blank=True)
    ordering = models.PositiveIntegerField("Порядок в меню", db_index=True)
    main_menu = models.BooleanField("Главное меню", db_index=True, default=False)
    is_limited = models.BooleanField("Ограничение запросов", db_index=True, default=False,
                                     help_text="Включает ограничение частоты выполнения запросов данной кнопкой")
    time_limit = models.PositiveIntegerField("Время ограничения, сек", blank=True, null=True)
    active = models.BooleanField("Активная", db_index=True, default=True)

    objects = models.Manager()
    active_button_objects = ActiveButtonObjects()

    def get_absolute_url(self):
        return "/bot/buttons/" + str(self.id)

    def get_submenu_buttons(self):
        """
        Формирование перечня кнопок подменю, привязанных к кнопке
        :return:
        """
        return "".join(['<p><a href="' + str(p.pk) + '">' + p.text + '</a></p>' for p in self.submenu.all()])

    get_submenu_buttons.allow_tags = True
    get_submenu_buttons.short_description = 'Подменю'

    def button_actions(self):
        """
        Формирование перечня действия, привязанных к кнопке
        :return: html-инклюд
        """
        return "".join(
            ['<p><a href="' + Actions.get_absolute_url(a) + '">' + a.name + '</a></p>' for a in self.actions.all()])

    button_actions.allow_tags = True
    button_actions.short_description = 'Подключенные экшены'

    @staticmethod
    def get_actions(button_id):
        """
        Получения перечня всех действий, привязанных к кнопке
        :param button_id:
        :return: list
        """
        return Buttons.active_button_objects.get(pk=button_id).actions.all()

    @staticmethod
    def get_interactive_actions(button_id):
        return Buttons.active_button_objects.get(pk=button_id).actions.filter(is_interactive=True)

    @staticmethod
    def get_not_interactive_actions(button_id):
        return Buttons.active_button_objects.get(pk=button_id).actions.filter(is_interactive=False)

    @staticmethod
    def get_message(button_id):
        return Buttons.active_button_objects.get(pk=button_id).message

    @staticmethod
    def get_inline_menu(button_id):
        """
        Собрать inline-подменю для кнопки
        :param button_id:
        :return: inline-клавиатура
        """
        buttons = Buttons.active_button_objects.get(pk=button_id).submenu.all().order_by("ordering")
        inline_keyboard = InlineKeyboardMarkup(
            k.lining_keyboard(
                [InlineKeyboardButton((e.FILE_FOLDER if Buttons.has_submenu(button.id) else e.RADIO_BUTTON) +
                                      " " +
                                      button.text,
                                      callback_data=json.dumps({"button_id": str(button.id)}),
                                      )
                 for button in buttons]
            )
        )
        return inline_keyboard

    @staticmethod
    def get_main_menu(chat_id):
        """
        Получение разметки клавиатуры главного меню
        :param chat_id:
        :return: inline-клавиатура
        """
        # это ахтунг, но это самый быстрый способ без прямого запроса в бд
        buttons = sorted(
            list(
                set(
                    sum([list(g.menu.all()) for g in BotUser.active_users.get(chat_identifier=chat_id).user_groups.all()], [])
                )
            ),
            key=lambda k: k.ordering)

        button_array_for_markup = k.lining_keyboard([InlineKeyboardButton(
                (e.FILE_FOLDER if Buttons.has_submenu(button.id) else e.RADIO_BUTTON) +
                " " +
                button.text,
                callback_data=json.dumps({"button_id": str(button.id)}))
                for button in buttons])
        if BotUser.active_users.get(chat_identifier=chat_id).subscriptions_on:

            # если пользователю подключены подписки, то в основном меню выводим дополнительную кнопку
            button_array_for_markup.append([InlineKeyboardButton(e.CLIPBOARD + " " + "Подписки",
                                                                 callback_data=json.dumps({"subscriptions_button": True}))])

        inline_keyboard = InlineKeyboardMarkup(button_array_for_markup)
        return inline_keyboard

    @staticmethod
    def has_submenu(menu_id):
        """
        Проверка наличия подменю у кнопки
        :param menu_id:
        :return: Bool
        """
        return bool(Buttons.active_button_objects.get(pk=menu_id).submenu.all())

    @staticmethod
    def check_timeout_execute_permission(button_id, chat_id):
        """
        Проверка возможности выполнения экшенов, привязанных к кнопке (по таймауту)
        :param button_id:
        :param chat_id:
        :return: Bool, время до наступления разрешения
        """
        button = Buttons.objects.get(pk=button_id)
        button_time_limit = button.time_limit if button.time_limit else 0
        obj, is_created = UserButtonAdditionalData.objects.get_or_create(
            bot_user=BotUser.active_users.get(chat_identifier=chat_id),
            button=button)
        if button.is_limited:
            sec_from_exec = timezone.now().timestamp() - obj.last_activity_datetime.timestamp()
            if button_time_limit > sec_from_exec:
                return [False, button_time_limit - sec_from_exec]
        return [True, None]

    @staticmethod
    def fix_button_press(button_id, chat_id):
        """
        Обновление времени нажатия кнопки
        :param button_id:
        :param chat_id:
        :return:
        """
        add_data = UserButtonAdditionalData.objects.get(
            bot_user=BotUser.active_users.get(chat_identifier=chat_id),
            button=Buttons.objects.get(pk=button_id))
        add_data.last_activity_datetime = timezone.now()
        add_data.save()

    class Meta:
        db_table = 'menu_buttons'
        verbose_name = "кнопку меню"
        verbose_name_plural = "Кнопки меню"


class ActiveUserGroupObjects(models.Manager):
    def get_queryset(self):
        return super(ActiveUserGroupObjects, self).get_queryset().filter(active=True)


class UserGroup(models.Model):

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    name = models.CharField("Наименование группы", max_length=100)
    menu = models.ManyToManyField(Buttons,
                                  verbose_name="Меню, доступные группе",
                                  related_name="allow_menu",
                                  blank=True)

    default = models.BooleanField("Подключается по умолчанию при принятии инвайта", db_index=True, default=False)
    active = models.BooleanField("Активная", db_index=True, default=True)

    def menu_list(self):
        """
        Перечень подключенных меню
        :return: html инклюд
        """
        array = list(self.menu.all())
        tmp_array = []
        for _ in range(6):
            try:
                b = array.pop(0)
                tmp_array.append(h.Html(b.text).a(b.get_absolute_url()))
            except IndexError:
                pass
        if array:
            return ", ".join(tmp_array) + "..."
        return ", ".join(tmp_array)

    menu_list.allow_tags = True
    menu_list.short_description = "Меню"

    objects = models.Manager()
    active_user_group_objects = ActiveUserGroupObjects()

    def get_absolute_url(self):
        return "/bot/usergroup/" + str(self.id)

    @staticmethod
    def get_default_groups():
        return UserGroup.active_user_group_objects.filter(default=True)

    class Meta:
        db_table = "user_group"
        verbose_name = "группа пользователей"
        verbose_name_plural = "Группы пользователей"


class SubscriptionType(models.Model):

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    name = models.CharField("Наименование", max_length=100)

    class Meta:
        db_table = 'subscription_type'
        verbose_name = "тип подписки"
        verbose_name_plural = "Типы подписок"


class ActiveSubscriptionObjects(models.Manager):
    def get_queryset(self):
        return super(ActiveSubscriptionObjects, self).get_queryset().filter(active=True)


class Days(models.Model):
    name = models.CharField("День", max_length=20)
    index = models.IntegerField("Индексный номер", db_index=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    class Meta:
        db_table = 'week_days'
        verbose_name = "дни недели"
        verbose_name_plural = "Дни недели"
        ordering = ('pk',)


class Subscriptions(models.Model):
    # подписки, созданные оператором
    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    type = models.ForeignKey(SubscriptionType, on_delete=models.CASCADE, verbose_name="Тип подписки")
    name = models.CharField("Наименование подписки", max_length=100)
    codename = models.CharField("Код подписки", max_length=50,
                                validators=[RegexValidator(regex='^[0-9A-z_]+$',
                                                           message="Допустимы английские символы, цифры, подчеркивание")],
                                help_text="Код подписки, английские буквы, цифры, подчеркивание"
                                , db_index=True)
    information = models.TextField("Информация о подписке")
    actions = models.ManyToManyField(Actions,
                                     symmetrical=False,
                                     verbose_name="действия",
                                     related_name="actions")

    is_periodical = models.BooleanField("Периодическая", default=False, db_index=True)
    interval = models.IntegerField("Интервал", default=600, help_text="Интервал выполнения (секунд)")
    indent = models.IntegerField("Отступ", default=0, help_text="Задержка первого выполнения (секунд)")

    is_daily = models.BooleanField("Ежедневная", default=False, db_index=True)
    exec_days = models.ManyToManyField(Days,
                                       symmetrical=False,
                                       verbose_name="дни недели",
                                       related_name="week_days",
                                       null=True,
                                       blank=True)

    active = models.BooleanField("Активная", default=True, db_index=True)

    objects = models.Manager()
    active_subscription_objects = ActiveSubscriptionObjects()

    def exec_day_list(self):
        """
        Получение перечня дней выполнения подписки
        :return: html-инклюд
        """
        days = self.exec_days.all()
        if len(days) == 7:
            return "Ежедневно"
        if sorted([d.name for d in days]) == sorted(["Понедельник", "Вторник", "Среда", "Четверг", "Пятница"]):
            return "Будние дни"
        return "".join([("<p>" + d.name + "</p>") for d in self.exec_days.all()])

    exec_day_list.allow_tags = True
    exec_day_list.short_description = "Дни"

    def exec_time_list(self):
        """
        Получение списка времени выполнения подписки
        :return: html-инклюд
        """
        array = list(self.subscriptionexecutetime_set.all())
        tmp_array = []
        for _ in range(2):
            try:
                t = array.pop(0)
                tmp_array.append(h.Html(t.exec_time).a(t.get_absolute_url()))
            except IndexError:
                pass
        if array:
            return ", ".join(tmp_array) + "..."
        return ", ".join(tmp_array)

    exec_time_list.allow_tags = True
    exec_time_list.short_description = "Время выполнения"

    class Meta:
        db_table = 'subscriptions'
        verbose_name = "подписку"
        verbose_name_plural = "Подписки"


class SubscriptionExecuteTime(models.Model):
    # таблица дней недели
    subscription = models.ForeignKey(Subscriptions,
                                     on_delete=models.CASCADE,
                                     verbose_name="Подписка")
    exec_time = models.TimeField("Время выполнения")

    def __str__(self):
        return str(self.exec_time)

    def __repr__(self):
        return self.__str__()

    def get_absolute_url(self):
        return "/bot/subscriptionexecutetime/" + str(self.id)

    class Meta:
        db_table = 'subscription_execute_time'
        verbose_name = "время запуска подписки"
        verbose_name_plural = "Время запуска подписки"
        ordering = ("subscription", 'exec_time',)


class ActiveBotUser(models.Manager):
    def get_queryset(self):
        return super(ActiveBotUser, self).get_queryset().filter(active=True)


class BotUser(models.Model):

    def __str__(self):
        return self.get_full_name()

    def __repr__(self):
        return self.__str__()

    def get_full_name(self):
        return self.surname + " " + self.name

    get_full_name.short_description = 'Пользователь'

    surname = models.CharField("Фамилия", max_length=100)
    name = models.CharField("Имя", max_length=100)
    second_name = models.CharField("Отчество", max_length=100, blank=True, null=True)
    email = models.EmailField("Почтовый адрес", blank=True, null=True)
    user_groups = models.ManyToManyField(UserGroup,
                                         verbose_name="Группы пользователя",
                                         related_name="user_group",
                                         blank=True)
    chat_identifier = models.CharField("Идентификатор чата", max_length=40, db_index=True)
    user_code_pass = models.CharField("Кодовое слово", max_length=50)
    organization_inn = models.CharField("ИНН", max_length=20)
    organization_kpp = models.CharField("КПП", max_length=20, blank=True, null=True)
    subscriptions_on = models.BooleanField("Включить подписки", default=False)
    active_subscriptions = models.ManyToManyField(Subscriptions,
                                                  verbose_name="Активные подписки пользователя",
                                                  related_name="active_user",
                                                  blank=True)
    allow_subscriptions = models.ManyToManyField(Subscriptions,
                                                 verbose_name="Доступные пользователю подписки",
                                                 related_name="allow_user",
                                                 blank=True)
    last_button_callback_json = models.CharField("Данные последней нажатой кнопки",
                                                 max_length=50,
                                                 blank=True,
                                                 null=True)
    active = models.BooleanField("Активный", default=True, db_index=True)

    objects = models.Manager()
    active_users = ActiveBotUser()

    @staticmethod
    def is_registered(chat_id):
        """
        Проверка, зарегистрирован ли пользователь
        :param chat_id:
        :return: Bool
        """
        return bool(BotUser.active_users.filter(chat_identifier=chat_id))

    @staticmethod
    def get_user(chat_id):
        """
        Получить объект пользователя
        :param chat_id:
        :return:
        """
        return BotUser.active_users.filter(chat_identifier=chat_id)

    @staticmethod
    def get_user_appeal(chat_id):
        """
        Обращение к пользователю (имя и фамилия)
        :param chat_id:
        :return: str
        """
        u = BotUser.active_users.get(chat_identifier=chat_id)
        s = u.name + ((" " + u.second_name) if u.second_name else "")
        return s if s else "пользователь"

    def get_user_group(self):
        """
        Список подключенных пользователю групп
        :return: html-инклюд
        """
        return "".join(['<p><a href="' + g.get_absolute_url() + '">' + str(g) + '</a></p>' for g in self.user_groups.all()])

    get_user_group.allow_tags = True
    get_user_group.short_description = 'Группы'

    @staticmethod
    def get_subscriptions_markup(chat_id):
        """
        Формирование inline-клавиатуры для менеджера подписок
        :param chat_id:
        :return:
        """
        allow_subscriptions_arr = BotUser.active_users.get(chat_identifier=chat_id).allow_subscriptions.all()
        active_subscriptions_arr = BotUser.active_users.get(chat_identifier=chat_id).active_subscriptions.all()
        button_array_for_markup = [[
            InlineKeyboardButton((e.PUSHPIN if subscription in active_subscriptions_arr else e.EMPTY)
                                 + " "
                                 + subscription.name,
                                 callback_data=json.dumps({"subscription_id": str(subscription.id)})),
            InlineKeyboardButton("Инфо", callback_data=json.dumps({"subscription_info_id": str(subscription.id)}))]
            for subscription in allow_subscriptions_arr]

        button_array_for_markup.append(
            [InlineKeyboardButton(e.CROSS_MARK + " Закрыть менеджер",
                                  callback_data=json.dumps({"exit_subscriptions_button": True})), ]
        )

        inline_keyboard = InlineKeyboardMarkup(button_array_for_markup)
        return inline_keyboard

    @staticmethod
    def off_user_subscription(chat_id, subscription_id):
        """
        Отключение подписки из списка разрешенных пользователю
        :param chat_id:
        :param subscription_id:
        :return:
        """
        BotUser.active_users.get(chat_identifier=chat_id).active_subscriptions.remove(
            Subscriptions.active_subscription_objects.get(pk=subscription_id)
        )

    @staticmethod
    def on_user_subscription(chat_id, subscription_id):
        """
        Включение подписки из списка разрешенных пользователю
        :param chat_id:
        :param subscription_id:
        :return:
        """
        BotUser.active_users.get(chat_identifier=chat_id).active_subscriptions.add(
            Subscriptions.active_subscription_objects.get(pk=subscription_id)
        )

    @staticmethod
    def subscription_id_active_for_user(chat_id, subscription_id):
        return bool(BotUser.active_users.get(chat_identifier=chat_id).active_subscriptions.filter(pk=subscription_id))

    @staticmethod
    def get_subscription_info(chat_id, subscription_id):
        subscription = BotUser.active_users.get(chat_identifier=chat_id).allow_subscriptions.get(pk=subscription_id)
        return h.Html(subscription.name + ": ").b() + subscription.information

    def get_absolute_url(self):
        return str(self.id)

    class Meta:
        db_table = 'bot_user'
        verbose_name = "пользователя"
        verbose_name_plural = "Пользователи"


class UserSubscriptionsAdditionalData(models.Model):
    bot_user = models.ForeignKey(BotUser, on_delete=models.CASCADE, verbose_name="Пользователь")
    subscription = models.ForeignKey(Subscriptions, on_delete=models.CASCADE, verbose_name="Подписка")
    start_datetime = models.DateTimeField("Дата и время начала диапазона", blank=True, null=True)
    end_datetime = models.DateTimeField("Дата и время окончания диапазона", blank=True, null=True)

    class Meta:
        db_table = 'user_subscription_additional_data'
        verbose_name = "дополнительной информации"
        verbose_name_plural = "Дополнительная информация"


class UserButtonAdditionalData(models.Model):
    bot_user = models.ForeignKey(BotUser, on_delete=models.CASCADE, verbose_name="Пользователь")
    button = models.ForeignKey(Buttons, on_delete=models.CASCADE, verbose_name="Кнопка")
    last_activity_datetime = models.DateTimeField("Дата и время последнего использования кнопки",
                                                  default=timezone.datetime(2000, 1, 1, 0, 0, 0, 0, tzinfo=pytz.UTC)
                                                  # default=timezone.make_naive(
                                                  #    timezone.datetime(2000, 1, 1, 0, 0, 0, 0))
    )

    class Meta:
        db_table = 'user_button_additional_data'
        verbose_name = "дополнительной информации"
        verbose_name_plural = "Дополнительная информация"


class ActiveInvites(models.Manager):
    def get_queryset(self):
        return super(ActiveInvites, self).get_queryset().filter(active=True)


class Invites(models.Model):

    def __str__(self):
        return self.email

    def __repr__(self):
        return self.__str__()

    def get_full_name(self):
        s = (self.surname if self.surname else "") + " " + (self.name if self.name else "")
        return "-" if s == " " else s

    get_full_name.short_description = 'Пользователь'

    email = models.EmailField("Почтовый адрес", unique=True)
    chat_identifier = models.CharField("Идентификатор чата", max_length=40, blank=True, null=True, db_index=True)
    name = models.CharField("Имя", max_length=100, blank=True, null=True)
    surname = models.CharField("Фамилия", max_length=100, blank=True, null=True)
    invite_code = models.CharField("Код инвайта", max_length=20, db_index=True)
    user_code_pass = models.CharField("Кодовое слово", max_length=50)
    organization_inn = models.CharField("ИНН", max_length=20, blank=True, null=True)
    approve_by_user = models.BooleanField("Подтвержден пользователем", default=False, db_index=True)
    approve_by_manager = models.BooleanField("Подтвержден менеджером", default=False, db_index=True)
    create_datetime = models.DateTimeField("Дата создания инвайта", auto_now_add=True)
    refused = models.BooleanField("Отказ в регистрации", default=False, db_index=True)
    user_created = models.BooleanField("Создан пользователь", default=False, db_index=True)
    active = models.BooleanField("Активный", default=True, db_index=True)

    objects = models.Manager()
    active_invites = ActiveInvites()

    class Meta:
        db_table = 'user_invites'
        verbose_name = "инвайты"
        verbose_name_plural = "Инвайты"

    @staticmethod
    def get_invite(**kwargs):
        invite = None
        try:
            if kwargs.get("invite_code"):
                invite = Invites.active_invites.get(invite_code=kwargs.get("invite_code"),
                                                    approve_by_user=False,
                                                    approve_by_manager=False)

            elif kwargs.get("chat_identifier"):
                invite = Invites.active_invites.get(chat_identifier=kwargs.get("chat_identifier"),
                                                    approve_by_user=False,
                                                    approve_by_manager=False)
        except ObjectDoesNotExist:
            pass
        return invite


# сигнал изменения активных подписок
m2m_changed.connect(s.active_subscriptions_changed, sender=BotUser.active_subscriptions.through,
                    dispatch_uid="active_subscriptions_changed_unique_identifier")
post_save.connect(s.invite_changed, sender=Invites, dispatch_uid="invite_changed_unique_identifier")




