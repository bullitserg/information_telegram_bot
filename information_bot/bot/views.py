import bot.models as models
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.views.generic import View


# Create your views here.


class InviteApproveView(UserPassesTestMixin, LoginRequiredMixin, View):
    """
    Вью обработки нажатия кнопки подтверждения инвайта
    """

    def test_func(self):
        """
        Тестирование полномочий для UserPassesTestMixin
        :return:
        """
        if (not self.request.user.is_staff) or (not self.request.user.is_superuser):
            return False
        return True

    def get(self, request, *args, **kwargs):
        invite_id = kwargs.get("invite_id")
        invite = models.Invites.active_invites.get(id=invite_id)

        if not invite.approve_by_user:
            messages.error(request, "Необходимо подтверждение пользователя")
        else:
            invite.approve_by_manager = True
            # указываем update_field, чтобы инициировать соответствующий сигнал
            invite.save(update_fields=['approve_by_manager'])
            messages.success(request, f"Пользователь {invite.get_full_name()} подтвержден")
        # загадка природы, какого черта это не работает
        # return HttpResponseRedirect(reverse("admin:bot_invites_changelist"))
        return HttpResponseRedirect("/bot/invites/")


