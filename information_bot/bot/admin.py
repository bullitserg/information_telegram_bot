import bot.components.invite as invite
import bot.components.about as a
import bot.forms as forms
import bot.models as models
from django.conf import settings
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils.html import format_html

from .models import Buttons, UserGroup, SubscriptionType, \
    Subscriptions, BotUser, Actions, SubscriptionExecuteTime, BotAdministrator, Invites


class ModelAdminWithAbout(admin.ModelAdmin):
    """
    Добавление описания в контект. Выводится в правом блоке при редактировании через about_obj
    """
    about_obj = None

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        context.update({"about_obj": self.about_obj})
        return super().render_change_form(request, context, add, change, form_url, obj)


class MyAdminSite(admin.AdminSite):
    site_header = 'Администрирование Telegram-bot'
    site_url = None

    def index(self, request, extra_context=None):
        invite_form = forms.InviteForm(request.POST)
        if request.POST:
            if invite_form.is_valid():
                cn = invite_form.clean()
                user_invite = invite.Invite(cn['email'])
                status, error = user_invite.create_invite()
                if status:
                    messages.success(request, f"Отправлен инвайт на адрес {cn['email']}")
                else:
                    messages.success(request, error)
            else:
                messages.error(request, "Введите правильный адрес электронной почты.")
        return super(MyAdminSite, self).index(request, extra_context={"bot_username": settings.BOT_USERNAME,
                                                                      "logo_url": settings.LOGO_URL,
                                                                      "invite_form": invite_form,
                                                                      "main_info_html": a.main_info_html,
                                                                      "models_info_html": a.models_info_html()})


class SubscriptionExecuteTimeInstanceInline(admin.TabularInline):
    model = SubscriptionExecuteTime


class SubscriptionAdmin(ModelAdminWithAbout):
    about_obj = a.info.subscription
    list_display = ["name", "type", "is_periodical", "interval",
                    "is_daily", "exec_time_list", "exec_day_list", "active", ]
    list_filter = ["is_periodical", "is_daily", "active", ]
    filter_horizontal = ["actions", "exec_days", ]
    search_fields = ["name", ]
    list_per_page = 25
    inlines = [SubscriptionExecuteTimeInstanceInline]

    fieldsets = (
        ("Подписка", {
            'classes': ('wide', 'extrapretty', ),
            'fields': ('type', 'name', 'codename', 'information', 'actions', 'active')
        }),
        ("Периодическое выполнение", {
            'classes': ('collapse', 'wide', 'extrapretty', ),
            'fields': ('is_periodical', 'interval', 'indent')
        }),
        ('Ежедневное выполнение', {
            'classes': ('collapse', 'wide', 'extrapretty', ),
            'fields': ('is_daily', 'exec_days', )
        }),
    )
    change_form_template = 'admin/subscriptions_change_form.html'


class BotUserAdmin(ModelAdminWithAbout):
    about_obj = a.info.bot_user
    list_display = ["get_full_name", "chat_identifier", "get_user_group", 'subscriptions_on', "active", ]
    filter_horizontal = ["user_groups", "active_subscriptions", "allow_subscriptions", ]
    list_filter = ["active", 'subscriptions_on', ]
    search_fields = ["surname", "name", "chat_identifier", 'organization_inn']
    list_per_page = 25
    fieldsets = (
        ("Пользователь", {
            'classes': ('wide', 'extrapretty'),
            'fields': ('surname', 'name', 'second_name', 'chat_identifier', 'user_code_pass', 'email', 'active')
        }),
        ("Организация", {
            'classes': ('wide', 'extrapretty'),
            'fields': ('organization_inn', 'organization_kpp')
        }),
        ('Группы пользователя', {
            'classes': ('collapse', 'wide', 'extrapretty', ),
            'fields': ('user_groups', )
        }),
        ('Подписки', {
            'classes': ('collapse', 'wide', 'extrapretty', ),
            'fields': ('subscriptions_on', 'allow_subscriptions', 'active_subscriptions'),
        }),
    )

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        """
        Фильтр для выборки в filter_horizontal
        :param db_field:
        :param request:
        :param kwargs:
        :return:
        """
        if db_field.name == "menu":
            # в queryset помещаем кнопки помеченные "главное меню"
            kwargs['queryset'] = Buttons.active_button_objects.filter(main_menu=True)

        if db_field.name == "active_subscriptions":

            # если элементов доступных пользователю меню пока нет, то просто оставляем все как есть
            try:
                user_id = request.resolver_match.args[0]
            except IndexError:
                return super().formfield_for_manytomany(db_field, request, **kwargs)

            # в queryset помещаем только доступные пользователю меню
            kwargs['queryset'] = BotUser.active_users.get(pk=user_id).allow_subscriptions.filter(active=True)
        return super().formfield_for_manytomany(db_field, request, **kwargs)


class ButtonsAdmin(ModelAdminWithAbout):
    about_obj = a.info.buttons
    list_display = ["text", "ordering", "main_menu", "get_submenu_buttons", "button_actions", "is_limited", "time_limit", "active", ]
    filter_horizontal = ["submenu", "actions"]
    ordering = ["-active", "-main_menu", "ordering", ]
    list_filter = ["main_menu", 'is_limited', "active", ]
    search_fields = ["text", ]
    list_per_page = 25
    list_editable = ["ordering", ]
    fieldsets = (
        ("Кнопка", {
            'classes': ('wide', 'extrapretty'),
            'fields': (('text', 'message'), 'ordering', 'main_menu',  'active', )
        }),
        ("Ограничения", {
            'classes': ('collapse',),
            'fields': ('is_limited', 'time_limit', )
        }),
        ('Подменю', {
            'classes': ('collapse', ),
            'fields': ('submenu', )
        }),
        ('Экшены', {
            'classes': ('collapse', ),
            'fields': ('actions', ),
        }),
    )

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        """
        Фильтр для выборки в filter_horizontal
        :param db_field:
        :param request:
        :param kwargs:
        :return:
        """
        if db_field.name == "submenu":

            # если элементов меню пока нет, то просто оставляем все как есть
            try:
                button_id = request.resolver_match.args[0]
            except IndexError:
                return super().formfield_for_manytomany(db_field, request, **kwargs)

            # в queryset помещаем кнопки за исключением текущей
            kwargs['queryset'] = Buttons.active_button_objects.exclude(pk=button_id)

        return super().formfield_for_manytomany(db_field, request, **kwargs)


class ActionsAdmin(ModelAdminWithAbout):
    about_obj = a.info.actions
    list_display = ["name", "is_interactive", "active", ]
    list_filter = ["is_interactive", "active", ]
    list_per_page = 25
    search_fields = ["name", ]


class UserGroupAdmin(ModelAdminWithAbout):
    about_obj = a.info.user_group
    list_display = ["name", "menu_list", "default", "active", ]
    filter_horizontal = ["menu", ]
    list_per_page = 25

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        """
        Фильтр для выборки в filter_horizontal
        :param db_field:
        :param request:
        :param kwargs:
        :return:
        """
        if db_field.name == "menu":
            # в queryset помещаем только кнопки главного меню
            kwargs['queryset'] = Buttons.active_button_objects.filter(main_menu=True)

        return super().formfield_for_manytomany(db_field, request, **kwargs)


class InvitesAdmin(ModelAdminWithAbout):
    about_obj = a.info.invites
    list_display = ["email", "invite_code", "chat_identifier", "get_full_name", "organization_inn",
                    "create_datetime", "active", "approve_invite", ]

    list_filter = ["approve_by_user", "approve_by_manager",  "refused", "active", ]

    search_fields = ["subscription", "name", "surname", "organization_inn", ]
    list_per_page = 25

    def has_add_permission(*args, **kwargs): return False

    def save_model(self, request, obj, form, change):
        obj.save()
        # и с аргументом approve_by_manager для вызова сигнала
        obj.save(update_fields=['approve_by_manager'])

    fieldsets = (
        ("Данные пользователя", {
            'classes': ('wide', 'extrapretty', ),
            'fields': ('chat_identifier', 'email',  ('name', 'surname'), 'organization_inn', 'active', )
        }),
        ("Коды", {
            'classes': ('collapse', 'wide', 'extrapretty', ),
            'fields': ('user_code_pass', 'invite_code', )
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        """
        Hook for specifying custom readonly fields.
        Если уже по инвайту уже создан пользователь, то не имеет никакого смысла его редактировать
        """
        if obj.user_created:
            return ["chat_identifier", "email", "name", "name", "surname",
                    "organization_inn", "active", "user_code_pass", "invite_code",
                    "approve_by_user", "approve_by_manager", "approve_invite", ]
        else:
            return ["approve_invite", ]

    def approve_invite(self, obj):
        """
        Отрисовка поля подтверждения заявки (кнопки, отметки)
        :param obj:
        :return:
        """
        if obj.refused:
            button_html = '<i class="fas fa-exclamation-circle" style="color: red;"></i>'
        elif obj.approve_by_user and obj.approve_by_manager:
            button_html = format_html('<a class="button" href="{}{}">Пользователь</a>',
                                      "/bot/botuser/",
                                      models.BotUser.active_users.get(chat_identifier=obj.chat_identifier).pk, )
        elif not obj.approve_by_user:
            button_html = '<i class="fas fa-question-circle" style="color: #FFD700;;"></i>'
        else:
            button_html = format_html('<a class="button" style="background-color: #FF4500;" " href="{}">Подтвердить</a>',
                                      reverse('invite_approve', kwargs={"invite_id": obj.pk}), )
        return button_html

    approve_invite.short_description = ''
    approve_invite.allow_tags = True


class SubscriptionExecuteTimeAdmin(admin.ModelAdmin):
    list_display = ["exec_time", "subscription", ]
    list_filter = ["subscription", ]
    list_per_page = 25


admin_site = MyAdminSite(name="myadmin")

admin.site.unregister(User)


@admin.register(BotAdministrator)
class BotAdministratorAdmin(ModelAdminWithAbout, UserAdmin):
    about_obj = a.info.bot_administrator
    change_list_template = 'admin/change_list.html'
    list_display = ["get_full_name", "username", "email", "is_active", ]
    list_filter = ["is_active", ]
    list_per_page = 25
    search_fields = ["username", "first_name", "last_name", ]
    fieldsets = (
        ("Данные для входа", {
            'classes': ('wide', 'extrapretty',),
            'fields': ('username', 'password', )
        }),
        ("Сведения о пользователе", {
            'classes': ('wide', 'extrapretty',),
            'fields': ('first_name', 'last_name', 'email', 'is_active',)
        }),
    )

    def save_model(self, request, obj, form, change):
        # всем даем полные права по умолчанию
        obj.is_staff = 1
        obj.is_superuser = 1
        obj.save()


admin_site.register(Buttons, ButtonsAdmin)
admin_site.register(UserGroup, UserGroupAdmin)
admin_site.register(SubscriptionType)
admin_site.register(Subscriptions, SubscriptionAdmin)
admin_site.register(BotUser, BotUserAdmin)
admin_site.register(Actions, ActionsAdmin)
admin_site.register(SubscriptionExecuteTime, SubscriptionExecuteTimeAdmin)
admin_site.register(Invites, InvitesAdmin)
